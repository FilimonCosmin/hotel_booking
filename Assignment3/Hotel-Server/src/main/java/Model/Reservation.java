package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reservation")
public class Reservation {
    @Id
    @Column(name="name")
    String nume;
    @Column(name="nrroom")
    int nr;
    public Reservation(){

    }

    public Reservation(String nume, int nr) {
        this.nume = nume;
        this.nr = nr;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }
}

package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stars")
public class Rating {
    @Id
    @Column(name="name")
    private String nume;
    @Column(name="nrroom")
    private int nr;
    @Column(name = "stars")
    private  int stars;
    public Rating() {
        // this form used by Hibernate
    }

    public Rating(String nume, int nr, int stars) {
        this.nume = nume;
        this.nr = nr;
        this.stars = stars;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}

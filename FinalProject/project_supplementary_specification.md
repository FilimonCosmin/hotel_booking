Room Service Hotel Additional specification

# Introduction
[Introducing the supplementary specification can add an overview of an entire document.
The additional specification captures the requirements of the care systems are not easy to capture in the use cases of the use case model. Main requirements include:
Legal and regulatory requirements, including standards applied.
The quality attributes of the care system are to be built, including requirements for use, reliability, performance and supportability.
Other requirements, such as operating systems and environments, compatibility requirements and design restrictions.]

# Non-functional requirements

[Define the attributes of system quality in terms of scenarios, after tracking the setting:
- Definition of the quality attribute
- Source of the stimulus: User
- Stimulus: backup request to use
- Medium: Inactive
- Artifact: Search the database of rooms
- Answer: Check availability
- Response measure: Sending the answer is allowed
- Tactics
]
## Availability
Attribute definition: Attribute care ensures the application of this application
- Source of the stimulus: the user
- Stimulates:
* a component does not respond
- Environment: can be functional or non-functional - Artifact: complete system
- Answer: use determined by stimulus support
- Response measure: notification
## Performance
- Attribute definition: the time required for event liability
- Source of the stimulus: the user
- Stimulus: any external event.
- Environment: can be functional
- Artifact: total system
- Answer: use determined by stimulus support
- Response measure: the time required to process care events is coming.
## Security
- Attribute definition: measuring the ability to grant a resistance to unauthorized use
- The source of the stimulus: unauthorized person
- Stimulus: allows access to the date and system services
- Environment: can be functional
- Artifact: after.
- Answer: blocks access to the date and services
- Response measure: probability of detecting the attack
## Testability
- Attribute definition: specific test or entry
- Source of the stimulus: developer, tester, op
- Stimulation: test
- Environment: design, then
- Artifact: system
- Answer: it can be monitored and observed
- Response measure: coverage
## Usability
- Definition of attribute: is the degree of care can be used software - Source of stimulation: do they
- Stimulus: system
- Environment: runtime
- Artifact: total system
- Answer: offers ability
- Response measure: satisfaction of use
# Design constraints
The data to be applied will be stored throughout the date, using an ORM (relational mapping of the object) framework.
* Use a view-controller model (or view-view model if WPF is used) to design the application It is mandatory ** that the Controllers / ViewModels are do not want to refer directly to the View class (for example, tea for table care). at JFrame or JPanel), but there is an interface, if needed.
* Using the design model of a manufacturing method for generating reports (ie, selecting from the best reports for our care).
* All applicable entries will be valid for the invalid date prior to submission and save.
* Test the drive on ** controller (s) / view model (s) in relation to users using a test framework (eg JUnit, NUnit). Use a mocking framework (for example, Mockito, Moq) for a depot / visualization mock. The ** tests do not have to be necessary and we can access the database directly.

# Resources
* http://www.upedu.org/process/gdlines/md_srs.htm
* Example of additional specification: http://csis.pace.edu/~marchese/SE616_New/Samples/Example%20%2020Supplementary%20Specification.htm

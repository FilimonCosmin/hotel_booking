# Hotel Room Service Use Case Model

# Identification of use cases
[Identify actors, scenarios and use cases. Describe the three most important use cases according to the following format:

## Use case 1
* ** Use case **: Send room reservation request
* ** Level **: user objective level
* ** Lead actor **: User
* ** The main success scenario **: The administrator receives the request, verifies the availability and sends a response to the user.


## Use case 2
* ** Use case **: View available rooms and search by different conditions (size, type, price, etc.)
* ** Level **: user objective level.
* ** Lead actor **: User
* ** The main success scenario **: The administrator generates two types of report files, one in pdf format and one in txt or html format, with the available and reserved rooms that meet the conditions and send them to the user.


## Use case 3
* ** Use case **: Request to reserve the room after it is vacant
* ** Level **: User objective level
* ** Lead actor **: User
* ** The main success scenario **: The state of the room can have several values: free, busy, cleaning. The above notification is triggered only when it reaches "free"

## Use case 4
* ** Use case **: Note the camera
* ** Level **: user objective level
* ** Lead actor **: User
* ** The main success scenario **: The user can evaluate (between 1-10 stars) the last reservation of that room when it goes from "busy" to "cleaned". A room will thus have a list of previous customers who have used it, along with their ratings.

# UML Use case diagrams
[Create UML use case diagrams.]

## Diagram 1
! https://app.diagrams.net/#LUntitled%20Diagram.drawio


# Bibliography

* [Online diagram drawing software] (https://yuml.me/) ([Samples] (https://yuml.me/diagram/nofunky/usecase/samples))
* [Another Online Diagram Drawing Software] (https://www.draw.io)

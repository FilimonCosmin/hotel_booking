# Vision of Hotel Room Service

# Introduction
The purpose of the document is a collective, analyzed and defined the high level needs and characteristics of the hotel room reservation. They can be concentrated according to the necessary need for the parties and must be chosen by our country and there are still needs. The details may refer to the management module of the hotel reservation rooms, which are not available in case of use and in particular.

## The goal
The purpose of the document is a collective, analyzed and defined needs and to add to the high level reservation of hotel rooms.

## Scope to add
The purpose of the project is to implement an office application for the management of hotel care includes two types of users, a regular gain and an administrative administrator, care must provide a number of things and a word in order to use an application.

## Definitions, Acronyms and Abbreviations

* CRUD on rooms
* CRUD on information for regular users.

## References
https://en.wikipedia.org/wiki/Java_(software_platform)
## General presentation
See the document regarding a problem positioning statement, statement of posting a product, descriptor, stakeholder and user, with a summary for each, to be able to manage, library, and library.


# Positioning
## Problem statement

|||
| ---- | ------- |
| ** His problem ** | poorly organized information about room availability
| ** affects ** | the administrator of the buses.
| ** whose impact is ** | low efficiency
| ** a successful solution would be ** | a care application allows the use of available rooms to reserve them.

## The position statement of a product

|||
| ---- | ------- |
| ** For ** | administrator
| ** Who ** | need of need of the management system
| ** ** | the management of the reserved rooms is applicable
| ** That ** | ability to manage room reservation rooms
| ** Unlike ** | a manual, written
| ** Our product ** | Allows you to use the available rooms to be able to book more effectively

# Description of stakeholders and for a user

## Stakeholder summary
There are a number of stakeholders with interests and to help us be final.

### Stakeholders 1
* ** Name **: Architect
* ** Description **: main role in the application application.
* ** Responsibilities **: responsibility for the overall system architecture

### Stakeholders 2
* ** Name **: Developer
* ** Description **: runs the reservation systems of the rooms
* ** Responsibilities **: Implement the real application

## Summary of used

### User 1
* ** Name **: Regular user
* ** Description **: still in case of system
* ** Responsibilities **:
* List of available rooms for reservation
* Book or single room at a time, for a specified duration
* ** Stakeholders **: Self

### User 2
* ** Name **: Administrator
* ** Description **: Quick administration
* ** Responsibilities **:
* administration of available rooms
* Access common user information.
* ** Stakeholders **: Self

## Useful environment
The application will be used by users and the administrator.
The environment constraint is required to have the internet to be able to connect to the system server.

# Product requirements
Applications require a computer or laptop with an operating system installed and with
Java virtual machine installed or installed.
# Bibliography

- [Markdown online editor] (http://dillinger.io/)
- [Marking Tutorial] (https://www.markdowntutorial.com)
- [Markdown Markdown] (https://guides.github.com/features/mastering-markdown/)

package Controller;


import View.User;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.time.Instant;


public class UserController {
    private User user;
    public UserController(User user) throws IOException {
        this.user=user;
        user();
    }
    public static class Connection extends Thread
    {
        private final Socket socket;
        private final ObjectOutputStream output;
        private final ObjectInputStream input;

        public Connection(Socket socket) throws IOException
        {
            this.socket = socket;
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());

        }

        @Override
        public void run()
        {
            try
            {
                while (socket.isConnected())
                {
                    // Seems that input.available() is not reliable?
                    boolean serverHasData = socket.getInputStream().available() > 0;
                    if (serverHasData) {
                        String msg = (String) input.readObject();
                        System.out.println(Instant.now() + " Got from server: " + msg);
                    }

                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                System.out.println(Instant.now() + " Server disconnected");
            }
            catch (IOException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        public void sendMessageToServer(String message) throws IOException
        {
            output.writeObject(message);
        }
    }

    public void user() throws IOException
    {
        Connection connection = new Connection(new Socket("localhost", 3000));
        connection.start();

        //BufferedReader consoleInput = new BufferedReader(new InputStreamReader(System.in));
       user.getBtnNewButton().addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent actionEvent) {
               String pret=user.getFormattedTextField_2().getText();
               String size=user.getFormattedTextField_3().getText();
               String tip=user.getFormattedTextField_1().getText();
               try {
                   connection.sendMessageToServer("listRooms");
                   connection.sendMessageToServer(pret);
                   connection.sendMessageToServer(size);
                   connection.sendMessageToServer(tip);
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       });
            user.getBtnNewButton_1().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    String nr=user.getFormattedTextField().getText();
                    String din=user.getFormattedTextField_4().getText();
                    String dout=user.getFormattedTextField_5().getText();
                    String nume=user.getFormattedTextField_6().getText();
                    try {
                        connection.sendMessageToServer("addBooking");
                        connection.sendMessageToServer(nr);
                        connection.sendMessageToServer(din);
                        connection.sendMessageToServer(dout);
                        connection.sendMessageToServer(nume);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            user.getBtnNewButton_Rating().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    String nr=user.getFormattedTextField().getText();
                    String stars=user.getFormattedTextField_Stars().getText();
                    String nume=user.getFormattedTextField_6().getText();
                    String review=user.getReviewTextField().getText();
                    try {
                        connection.sendMessageToServer("addRating");
                        connection.sendMessageToServer(nr);
                        connection.sendMessageToServer(nume);
                        connection.sendMessageToServer(stars);
                        connection.sendMessageToServer(review);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        user.getBtnNewButton_Reserve().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nr=user.getFormattedTextField().getText();
                String nume=user.getFormattedTextField_6().getText();
                try {
                    connection.sendMessageToServer("addReservation");
                    connection.sendMessageToServer(nume);
                    connection.sendMessageToServer(nr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        user.getButtonShowReviews().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    connection.sendMessageToServer("listReviews");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}

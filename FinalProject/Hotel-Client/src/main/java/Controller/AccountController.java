package Controller;

import View.AdminCRUD;
import View.User;
import View.UserLogin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.time.Instant;

public class AccountController {
    private View.UserLogin userLogin;
    private static User user;
    private String n;
    private String p;

    public AccountController(UserLogin userLogin) throws IOException {
        this.userLogin = userLogin;
        userLogin.getBtnLogin().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    actionLogin();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        userLogin.getBtnSingUp().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    actionSingup();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

    }
    private void actionLogin() throws IOException {
        AccountController.Connection connection = new AccountController.Connection(new Socket("localhost", 3000));
        connection.start();
        String n=userLogin.getFormattedTextField().getText();
        String p=userLogin.getPasswordField().getText();


                    connection.sendMessageToServer("loginUser");
                    connection.sendMessageToServer(n);
                    connection.sendMessageToServer(p);

    }
    private void actionSingup() throws IOException {
        AccountController.Connection connection = new AccountController.Connection(new Socket("localhost", 3000));
        connection.start();
        String n=userLogin.getFormattedTextField().getText();
        String p=userLogin.getPasswordField().getText();


        connection.sendMessageToServer("CreateUser");
        connection.sendMessageToServer(n);
        connection.sendMessageToServer(p);
    }
    public static class Connection extends Thread
    {
        private final Socket socket;
        private final ObjectOutputStream output;
        private final ObjectInputStream input;
        //private User user;

        public Connection(Socket socket) throws IOException
        {
            this.socket = socket;
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());

        }

        @Override
        public void run()
        {
            try
            {
                while (socket.isConnected())
                {
                    // Seems that input.available() is not reliable?
                    boolean serverHasData = socket.getInputStream().available() > 0;
                    if (serverHasData) {
                        String msg = (String) input.readObject();
                        System.out.println(Instant.now() + " Got from server: " + msg);
                        fct(msg);
                    }

                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                System.out.println(Instant.now() + " Server disconnected");
            }
            catch (IOException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        private void fct(String msg) throws IOException {
            if(msg.equals("true")){
                user =new User();
                UserController crd=new UserController(user);
            }
            else
            {
                Component fail = null;
                if(msg.equals("false"))
                    JOptionPane.showMessageDialog(fail,"Invalid Username or Password");
            }
        }


        public void sendMessageToServer(String message) throws IOException
        {
            output.writeObject(message);
        }


    }


}

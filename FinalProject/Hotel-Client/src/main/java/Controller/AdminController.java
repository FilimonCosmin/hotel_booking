package Controller;


import View.AdminCRUD;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.time.Instant;



public class AdminController {

    private View.Administrator v;
    private AdminCRUD adminCRUD;

    public AdminController(View.Administrator v){
        this.v=v;
        v.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                try {
                    addaction();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void addaction() throws IOException {

                String nume = v.getFormattedTextField().getText();
                String pass = v.getPasswordField().getText();
                String actiune="login";
                Connection connection = null;
                try {
                    connection = new Connection(new Socket("localhost", 3000));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                connection.start();
                BufferedReader consoleInput = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Main app loop.");
                    System.out.println(Instant.now() + " Type the message to send to the server and press enter:");
                            connection.sendMessageToServer(actiune);
                            connection.sendMessageToServer(nume);
                            connection.sendMessageToServer(pass);



    }
    public class Connection extends Thread {
        private final Socket socket;
        private final ObjectOutputStream output;
        private final ObjectInputStream input;

        public Connection(Socket socket) throws IOException {
            this.socket = socket;
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());

        }

        @Override
        public void run() {
            try {
                while (socket.isConnected()) {
                    // Seems that input.available() is not reliable?
                    boolean serverHasData = socket.getInputStream().available() > 0;
                    if (serverHasData) {
                        String msg = (String) input.readObject();
                        System.out.println(Instant.now() + " Got from server: " + msg);
                        runadmin(msg);
                    }

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Instant.now() + " Server disconnected");
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        private void runadmin(String msg) throws IOException {
            if(msg.equals("true")){
                v.close();
                adminCRUD =new AdminCRUD();
                CRUDController crd=new CRUDController(adminCRUD);

            }
            else
            {
                Component fail = null;
                if(msg.equals("false"))
                    JOptionPane.showMessageDialog(fail,"Invalid Username or Password");
            }
        }

        public void sendMessageToServer(String message) throws IOException {
            output.writeObject(message);
        }
    }

}

package Controller;


import View.AdminCRUD;
import org.hibernate.SessionFactory;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.time.Instant;


public class CRUDController {
    private View.AdminCRUD adminCRUD;
    private JPanel report;
    public CRUDController(AdminCRUD adminCRUD) throws IOException {
        this.adminCRUD = adminCRUD;
        actions();

    }
    public static class Connection extends Thread
    {
        private final Socket socket;
        private final ObjectOutputStream output;
        private final ObjectInputStream input;

        public Connection(Socket socket) throws IOException
        {
            this.socket = socket;
            output = new ObjectOutputStream(socket.getOutputStream());
            input = new ObjectInputStream(socket.getInputStream());

        }

        @Override
        public void run()
        {
            try
            {
                while (socket.isConnected())
                {
                    // Seems that input.available() is not reliable?
                    boolean serverHasData = socket.getInputStream().available() > 0;
                    if (serverHasData) {
                        String msg = (String) input.readObject();
                        System.out.println(Instant.now() + " Got from server: " + msg);
                    }

                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
                System.out.println(Instant.now() + " Server disconnected");
            }
            catch (IOException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }

        public void sendMessageToServer(String message) throws IOException
        {
            output.writeObject(message);
        }
    }
    private void actions() throws IOException {
        Connection connection = new Connection(new Socket("localhost", 3000));
        connection.start();

        //BufferedReader consoleInput = new BufferedReader(new InputStreamReader(System.in));
//txtreport
        adminCRUD.getBtnNewButton_2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    connection.sendMessageToServer("txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //JOptionPane.showMessageDialog(report,"Report created!");
            }
        });
//pdfreport
        adminCRUD.getBtnNewButton_3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    connection.sendMessageToServer("pdf");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                   // JOptionPane.showMessageDialog(report, "Report created!");

            }
        });
//bookingedit
        adminCRUD.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nr=adminCRUD.getFormattedTextField().getText();
                String din=adminCRUD.getFormattedTextField_4().getText();
                String dout=adminCRUD.getFormattedTextField_5().getText();
                String nume=adminCRUD.getFormattedTextField_6().getText();
                String cond=adminCRUD.getFormattedTextField_7().getText();
                try {
                    connection.sendMessageToServer("editbooking");
                    connection.sendMessageToServer(nr);
                    connection.sendMessageToServer(din);
                    connection.sendMessageToServer(dout);
                    connection.sendMessageToServer(nume);
                    connection.sendMessageToServer(cond);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                JOptionPane.showMessageDialog(report,"Book edited!");
            }
        });

//roomedit
        adminCRUD.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nr=adminCRUD.getFormattedTextField().getText();
                String tip=adminCRUD.getFormattedTextField_1().getText();
                String pret=adminCRUD.getFormattedTextField_3().getText();
                String size=adminCRUD.getFormattedTextField_2().getText();

                try {
                    connection.sendMessageToServer("editroom");
                    connection.sendMessageToServer(nr);
                    connection.sendMessageToServer(tip);
                    connection.sendMessageToServer(pret);
                    connection.sendMessageToServer(size);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                JOptionPane.showMessageDialog(report,"Room edited!");

            }
        });
        adminCRUD.getBtnDeleteReservation().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nr=adminCRUD.getFormattedTextField().getText();
                //String nume=adminCRUD.getFormattedTextField_6().getText();
                try {
                    connection.sendMessageToServer("deleteReservation");
                    //connection.sendMessageToServer(nume);
                    connection.sendMessageToServer(nr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        adminCRUD.getBtnAcceptReservation().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nr=adminCRUD.getFormattedTextField().getText();
                //String nume=adminCRUD.getFormattedTextField_6().getText();
                try {
                    connection.sendMessageToServer("acceptReservation");
                    connection.sendMessageToServer(nr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        adminCRUD.getBtnShowReservation().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    connection.sendMessageToServer("showReservation");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

package View;

import javax.swing.*;
import java.awt.*;

public class User {

    private JFrame frame;
    private JTextField txtBooking;
    private JTextField txtRoomNumber;
    private JTextField txtType;
    private JTextField txtMaxPrice;
    private JTextField txtMinSize;
    private JTextField txtData;
    private JTextField txtCheckoutDate;
    private JTextField txtName;
    private JTextField txtStars;
    private JTextField txtRating;
    private JTextField txtPassword;
    private JTextField txtUsername;
    private JTextField txtReview;
    private JTextField txtCreateAccount;
    private JButton btnNewButton;
    private JButton btnNewButton_1;
    private JButton btnNewButton_Rating;
    private JButton btnNewButton_Reserve;
    private JButton ButtonShowReviews;
    private JButton ButtonSingIn;
    private JFormattedTextField formattedTextField;
    private JFormattedTextField formattedTextField_1;
    private JFormattedTextField formattedTextField_2;
    private JFormattedTextField formattedTextField_3;
    private JFormattedTextField formattedTextField_4;
    private JFormattedTextField formattedTextField_5;
    private JFormattedTextField formattedTextField_6;
    private JFormattedTextField formattedTextField_Stars;
    private JFormattedTextField ReviewTextField;
    private JTextPane textPane;
    public User() {
        initialize();
    }

    public JButton getBtnNewButton_Rating() {
        return btnNewButton_Rating;
    }

    public JTextField getTxtPassword() {
        return txtPassword;
    }

    public void setTxtPassword(JTextField txtPassword) {
        this.txtPassword = txtPassword;
    }

    public JTextField getTxtUsername() {
        return txtUsername;
    }

    public void setTxtUsername(JTextField txtUsername) {
        this.txtUsername = txtUsername;
    }

    public void setBtnNewButton_Rating(JButton btnNewButton_Rating) {
        this.btnNewButton_Rating = btnNewButton_Rating;
    }

    public JButton getBtnNewButton_Reserve() {
        return btnNewButton_Reserve;
    }

    public void setBtnNewButton_Reserve(JButton btnNewButton_Reserve) {
        this.btnNewButton_Reserve = btnNewButton_Reserve;
    }

    public JFormattedTextField getFormattedTextField_Stars() {
        return formattedTextField_Stars;
    }

    public void setFormattedTextField_Stars(JFormattedTextField formattedTextField_Stars) {
        this.formattedTextField_Stars = formattedTextField_Stars;
    }

    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(UIManager.getColor("Button.darkShadow"));
        frame.setBackground(UIManager.getColor("Button.focus"));
        frame.setBounds(100, 100, 950, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        txtBooking = new JTextField();
        txtBooking.setBackground(UIManager.getColor("Button.darkShadow"));
        txtBooking.setFont(new Font("Tahoma", Font.PLAIN, 22));
        txtBooking.setText("Booking");
        txtBooking.setBounds(12, 13, 94, 33);
        frame.getContentPane().add(txtBooking);
        txtBooking.setColumns(10);

        txtRoomNumber = new JTextField();
        txtRoomNumber.setBackground(UIManager.getColor("Button.darkShadow"));
        txtRoomNumber.setText("Room number:");
        txtRoomNumber.setBounds(12, 59, 116, 22);
        frame.getContentPane().add(txtRoomNumber);
        txtRoomNumber.setColumns(10);

        txtType = new JTextField();
        txtType.setBackground(UIManager.getColor("Button.darkShadow"));
        txtType.setText("Type:");
        txtType.setBounds(12, 94, 116, 22);
        frame.getContentPane().add(txtType);
        txtType.setColumns(10);

        txtMaxPrice = new JTextField();
        txtMaxPrice.setBackground(UIManager.getColor("Button.darkShadow"));
        txtMaxPrice.setText("Max price:");
        txtMaxPrice.setBounds(12, 129, 116, 22);
        frame.getContentPane().add(txtMaxPrice);
        txtMaxPrice.setColumns(10);

        txtMinSize = new JTextField();
        txtMinSize.setBackground(UIManager.getColor("Button.darkShadow"));
        txtMinSize.setText("Min size:");
        txtMinSize.setBounds(12, 164, 116, 22);
        frame.getContentPane().add(txtMinSize);
        txtMinSize.setColumns(10);

        txtData = new JTextField();
        txtData.setBackground(UIManager.getColor("Button.darkShadow"));
        txtData.setText("Check-in date:");
        txtData.setBounds(12, 199, 116, 22);
        frame.getContentPane().add(txtData);
        txtData.setColumns(10);

        txtCheckoutDate = new JTextField();
        txtCheckoutDate.setBackground(UIManager.getColor("Button.darkShadow"));
        txtCheckoutDate.setText("Check-out date:");
        txtCheckoutDate.setBounds(250, 199, 116, 22);
        frame.getContentPane().add(txtCheckoutDate);
        txtCheckoutDate.setColumns(10);

        formattedTextField = new JFormattedTextField();
        formattedTextField.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField.setBounds(140, 59, 330, 22);
        frame.getContentPane().add(formattedTextField);

        formattedTextField_1 = new JFormattedTextField();
        formattedTextField_1.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_1.setBounds(140, 94, 330, 22);
        frame.getContentPane().add(formattedTextField_1);

        formattedTextField_2 = new JFormattedTextField();
        formattedTextField_2.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_2.setBounds(140, 129, 330, 22);
        frame.getContentPane().add(formattedTextField_2);

        formattedTextField_3 = new JFormattedTextField();
        formattedTextField_3.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_3.setBounds(140, 164, 330, 22);
        frame.getContentPane().add(formattedTextField_3);

        formattedTextField_4 = new JFormattedTextField();
        formattedTextField_4.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_4.setBounds(140, 199, 98, 22);
        frame.getContentPane().add(formattedTextField_4);

        formattedTextField_5 = new JFormattedTextField();
        formattedTextField_5.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_5.setBounds(378, 199, 92, 22);
        frame.getContentPane().add(formattedTextField_5);

        btnNewButton = new JButton("Search");
        btnNewButton.setBackground(SystemColor.controlHighlight);
        btnNewButton.setBounds(12, 265, 116, 25);
        frame.getContentPane().add(btnNewButton);

        txtName = new JTextField();
        txtName.setBackground(UIManager.getColor("Button.darkShadow"));
        txtName.setText("Name:");
        txtName.setBounds(12, 234, 116, 22);
        frame.getContentPane().add(txtName);
        txtName.setColumns(10);

        formattedTextField_6 = new JFormattedTextField();
        formattedTextField_6.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_6.setBounds(140, 234, 330, 22);
        frame.getContentPane().add(formattedTextField_6);

        btnNewButton_1 = new JButton("Book room");
        btnNewButton_1.setBackground(SystemColor.controlHighlight);
        btnNewButton_1.setBounds(141, 265, 97, 25);
        frame.getContentPane().add(btnNewButton_1);

        //JTextPane textPane = new JTextPane();
        //textPane.setBackground(SystemColor.activeCaptionBorder);
        //textPane.setBounds(484, 94, 422, 162);
        //frame.getContentPane().add(textPane);

        txtRating=new JTextField();
        txtRating.setBackground(UIManager.getColor("Button.darkShadow"));
        txtRating.setText("Rating:");
        txtRating.setBounds(482, 59, 60, 22);
        frame.getContentPane().add(txtRating);
        txtRating.setColumns(10);

        formattedTextField_Stars = new JFormattedTextField();
        formattedTextField_Stars.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_Stars.setBounds(554, 59, 280, 22);
        frame.getContentPane().add(formattedTextField_Stars);

        txtStars = new JTextField();
        txtStars.setText("Stars");
        txtStars.setColumns(10);
        txtStars.setBackground(UIManager.getColor("Button.darkShadow"));
        txtStars.setBounds(846, 59, 60, 22);
        frame.getContentPane().add(txtStars);

        btnNewButton_Rating = new JButton("Add Rating");
        btnNewButton_Rating.setBounds(250, 265, 97, 25);
        btnNewButton_Rating.setBackground(SystemColor.controlHighlight);
        frame.getContentPane().add(btnNewButton_Rating);

        btnNewButton_Reserve = new JButton("Reserve");
        btnNewButton_Reserve.setBackground(SystemColor.controlHighlight);
        btnNewButton_Reserve.setBounds(359, 265, 97, 25);
        frame.getContentPane().add(btnNewButton_Reserve);

        txtReview = new JTextField();
        txtReview.setText("Review:");
        txtReview.setColumns(10);
        txtReview.setBackground(SystemColor.controlDkShadow);
        txtReview.setBounds(482, 94, 60, 22);
        frame.getContentPane().add(txtReview);

        ReviewTextField = new JFormattedTextField();
        ReviewTextField.setBackground(SystemColor.activeCaptionBorder);
        ReviewTextField.setBounds(554, 94, 280, 22);
        frame.getContentPane().add(ReviewTextField);

        ButtonShowReviews = new JButton("Show Reviews");
        ButtonShowReviews.setBackground(SystemColor.controlHighlight);
        ButtonShowReviews.setBounds(468, 265, 151, 25);
        frame.getContentPane().add(ButtonShowReviews);
        frame.setVisible(true);
    }

    public JButton getButtonShowReviews() {
        return ButtonShowReviews;
    }

    public void setButtonShowReviews(JButton buttonShowReviews) {
        ButtonShowReviews = buttonShowReviews;
    }

    public JFormattedTextField getReviewTextField() {
        return ReviewTextField;
    }

    public void setReviewTextField(JFormattedTextField reviewTextField) {
        ReviewTextField = reviewTextField;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public JFormattedTextField getFormattedTextField() {
        return formattedTextField;
    }

    public void setFormattedTextField(JFormattedTextField formattedTextField) {
        this.formattedTextField = formattedTextField;
    }

    public JFormattedTextField getFormattedTextField_1() {
        return formattedTextField_1;
    }

    public void setFormattedTextField_1(JFormattedTextField formattedTextField_1) {
        this.formattedTextField_1 = formattedTextField_1;
    }

    public JFormattedTextField getFormattedTextField_2() {
        return formattedTextField_2;
    }

    public void setFormattedTextField_2(JFormattedTextField formattedTextField_2) {
        this.formattedTextField_2 = formattedTextField_2;
    }

    public JFormattedTextField getFormattedTextField_3() {
        return formattedTextField_3;
    }

    public void setFormattedTextField_3(JFormattedTextField formattedTextField_3) {
        this.formattedTextField_3 = formattedTextField_3;
    }

    public JFormattedTextField getFormattedTextField_4() {
        return formattedTextField_4;
    }

    public void setFormattedTextField_4(JFormattedTextField formattedTextField_4) {
        this.formattedTextField_4 = formattedTextField_4;
    }

    public JFormattedTextField getFormattedTextField_5() {
        return formattedTextField_5;
    }

    public void setFormattedTextField_5(JFormattedTextField formattedTextField_5) {
        this.formattedTextField_5 = formattedTextField_5;
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    public JFormattedTextField getFormattedTextField_6() {
        return formattedTextField_6;
    }

    public void setFormattedTextField_6(JFormattedTextField formattedTextField_6) {
        this.formattedTextField_6 = formattedTextField_6;
    }

    public void write(String s){
        textPane.setText(s);
    }
}

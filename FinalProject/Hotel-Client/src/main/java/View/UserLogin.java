package View;
import java.awt.EventQueue;

import javax.swing.*;
import java.awt.SystemColor;
import java.awt.Font;

public class UserLogin {

    private JFrame frmHotelManager;
    private JTextField txtAdministratorLogin;
    private JTextField txtUserName;
    private JTextField txtPassword;
    private JPasswordField passwordField;
    private JButton BtnLogin;
    private JButton BtnSingUp;
    private JFormattedTextField formattedTextField;
    public UserLogin() {
        initialize();
    }

    private void initialize() {
        frmHotelManager = new JFrame();
        frmHotelManager.getContentPane().setBackground(UIManager.getColor("Button.darkShadow"));
        frmHotelManager.setBackground(SystemColor.desktop);
        frmHotelManager.setTitle("Hotel Client");
        frmHotelManager.setBounds(100, 100, 450, 300);
        frmHotelManager.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmHotelManager.getContentPane().setLayout(null);

        txtAdministratorLogin = new JTextField();
        txtAdministratorLogin.setBackground(UIManager.getColor("Button.darkShadow"));
        txtAdministratorLogin.setFont(new Font("Tahoma", Font.PLAIN, 22));
        txtAdministratorLogin.setText("User Login");
        txtAdministratorLogin.setBounds(12, 13, 210, 61);
        frmHotelManager.getContentPane().add(txtAdministratorLogin);
        txtAdministratorLogin.setColumns(10);

        txtUserName = new JTextField();
        txtUserName.setBackground(UIManager.getColor("Button.darkShadow"));
        txtUserName.setText("User Name:");
        txtUserName.setBounds(12, 87, 116, 22);
        frmHotelManager.getContentPane().add(txtUserName);
        txtUserName.setColumns(10);

        txtPassword = new JTextField();
        txtPassword.setBackground(UIManager.getColor("Button.darkShadow"));
        txtPassword.setText("Password:");
        txtPassword.setBounds(12, 122, 116, 22);
        frmHotelManager.getContentPane().add(txtPassword);
        txtPassword.setColumns(10);

        formattedTextField = new JFormattedTextField();
        formattedTextField.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField.setBounds(140, 87, 280, 22);
        frmHotelManager.getContentPane().add(formattedTextField);

        passwordField = new JPasswordField();
        passwordField.setBackground(SystemColor.activeCaptionBorder);
        passwordField.setBounds(140, 122, 280, 22);
        frmHotelManager.getContentPane().add(passwordField);

        BtnLogin = new JButton("Login");
        BtnLogin.setBackground(SystemColor.controlHighlight);
        BtnLogin.setBounds(12, 215, 97, 25);
        frmHotelManager.getContentPane().add(BtnLogin);
        frmHotelManager.setVisible(true);

        BtnSingUp = new JButton("Create");
        BtnSingUp.setBackground(SystemColor.controlHighlight);
        BtnSingUp.setBounds(125
                , 215, 97, 25);
        frmHotelManager.getContentPane().add(BtnSingUp);
        frmHotelManager.setVisible(true);
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(JPasswordField passwordField) {
        this.passwordField = passwordField;
    }

    public JButton getBtnLogin() {
        return BtnLogin;
    }

    public void setBtnLogin(JButton btnLogin) {
        BtnLogin = btnLogin;
    }

    public JButton getBtnSingUp() {
        return BtnSingUp;
    }

    public void setBtnSingUp(JButton btnSingUp) {
        BtnSingUp = btnSingUp;
    }

    public JFormattedTextField getFormattedTextField() {
        return formattedTextField;
    }

    public void setFormattedTextField(JFormattedTextField formattedTextField) {
        this.formattedTextField = formattedTextField;
    }
}



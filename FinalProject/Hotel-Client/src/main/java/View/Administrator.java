package View;


import javax.swing.*;
import java.awt.*;

public class Administrator {

    private JFrame frmHotelManager;
    private JTextField txtAdministratorLogin;
    private JTextField txtUserName;
    private JTextField txtPassword;
    private JPasswordField passwordField;
    private JButton btnNewButton;
    private JFormattedTextField formattedTextField;
    public Administrator() {
        initialize();
    }


    private void initialize() {
        frmHotelManager = new JFrame();
        frmHotelManager.getContentPane().setBackground(UIManager.getColor("Button.darkShadow"));
        frmHotelManager.setBackground(SystemColor.desktop);
        frmHotelManager.setTitle("Hotel Manager");
        frmHotelManager.setBounds(100, 100, 450, 300);
        frmHotelManager.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmHotelManager.getContentPane().setLayout(null);

        txtAdministratorLogin = new JTextField();
        txtAdministratorLogin.setBackground(UIManager.getColor("Button.darkShadow"));
        txtAdministratorLogin.setFont(new Font("Tahoma", Font.PLAIN, 22));
        txtAdministratorLogin.setText("Administrator Login");
        txtAdministratorLogin.setBounds(12, 13, 210, 61);
        frmHotelManager.getContentPane().add(txtAdministratorLogin);
        txtAdministratorLogin.setColumns(10);

        txtUserName = new JTextField();
        txtUserName.setBackground(UIManager.getColor("Button.darkShadow"));
        txtUserName.setText("User Name:");
        txtUserName.setBounds(12, 87, 116, 22);
        frmHotelManager.getContentPane().add(txtUserName);
        txtUserName.setColumns(10);

        txtPassword = new JTextField();
        txtPassword.setBackground(UIManager.getColor("Button.darkShadow"));
        txtPassword.setText("Password:");
        txtPassword.setBounds(12, 122, 116, 22);
        frmHotelManager.getContentPane().add(txtPassword);
        txtPassword.setColumns(10);

        formattedTextField = new JFormattedTextField();
        formattedTextField.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField.setBounds(140, 87, 280, 22);
        frmHotelManager.getContentPane().add(formattedTextField);

        passwordField = new JPasswordField();
        passwordField.setBackground(SystemColor.activeCaptionBorder);
        passwordField.setBounds(140, 122, 280, 22);
        frmHotelManager.getContentPane().add(passwordField);

        btnNewButton = new JButton("Login");
        btnNewButton.setBackground(SystemColor.controlHighlight);
        btnNewButton.setBounds(12, 215, 97, 25);
        frmHotelManager.getContentPane().add(btnNewButton);
        frmHotelManager.setVisible(true);
    }
    public void close(){
        frmHotelManager.setVisible(false);

    }
    public JFrame getFrmHotelManager() {
        return frmHotelManager;
    }

    public void setFrmHotelManager(JFrame frmHotelManager) {
        this.frmHotelManager = frmHotelManager;
    }

    public JTextField getTxtAdministratorLogin() {
        return txtAdministratorLogin;
    }

    public void setTxtAdministratorLogin(JTextField txtAdministratorLogin) {
        this.txtAdministratorLogin = txtAdministratorLogin;
    }

    public JTextField getTxtUserName() {
        return txtUserName;
    }

    public void setTxtUserName(JTextField txtUserName) {
        this.txtUserName = txtUserName;
    }

    public JTextField getTxtPassword() {
        return txtPassword;
    }

    public void setTxtPassword(JTextField txtPassword) {
        this.txtPassword = txtPassword;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(JPasswordField passwordField) {
        this.passwordField = passwordField;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public JFormattedTextField getFormattedTextField() {
        return formattedTextField;
    }

    public void setFormattedTextField(JFormattedTextField formattedTextField) {
        this.formattedTextField = formattedTextField;
    }
}
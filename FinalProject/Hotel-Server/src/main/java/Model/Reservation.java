package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="reservation")
public class Reservation {
    @Id
    @Column(name="id")
    int id;
    @Column(name="name")
    String nume;
    @Column(name="nrroom")
    int nr;
    @Column(name="status")
    String status;
    public Reservation(){

    }

    public Reservation(int id,String nume, int nr,String status) {
        this.id=id;
        this.nume = nume;
        this.nr = nr;
        this.status=status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }
}

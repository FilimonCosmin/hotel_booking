package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="room")
public class Room {
    @Id
    @Column(name="nr")
    private int nrRoom;
    @Column(name="type")
    private String type;
    @Column(name="rez")
    private String reserved;
    @Column(name="din")
    private String datain;
    @Column(name="dout")
    private String dataout;
    @Column(name="price")
    private  int price;
    @Column(name="size")
    private int size;
    @Column(name="name")
    private String nume;
    public Room(){

    }
    public Room(int nrRoom, String type, String reserved, String datain, String dataout, int price, int size,String nume) {
        this.nrRoom = nrRoom;
        this.type = type;
        this.reserved = reserved;
        this.datain = datain;
        this.dataout = dataout;
        this.price = price;
        this.size = size;
        this.nume=nume;
    }

    public int getNrRoom() {
        return nrRoom;
    }

    public void setNrRoom(int nrRoom) {
        this.nrRoom = nrRoom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReserved() {
        return reserved;
    }

    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    public String getDatain() {
        return datain;
    }

    public void setDatain(String datain) {
        this.datain = datain;
    }

    public String getDataout() {
        return dataout;
    }

    public void setDataout(String dataout) {
        this.dataout = dataout;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }
}

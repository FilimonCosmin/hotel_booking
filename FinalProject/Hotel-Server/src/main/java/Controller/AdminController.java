package Controller;

import Model.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import javax.swing.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;

import java.util.List;

public class AdminController {
    private static SessionFactory sessionFactory;
    public AdminController() throws IOException {
        addaction();
    }

    private void addaction() throws IOException {
        try (ServerSocket socket = new ServerSocket(3000))
        {
            while (true)
            {
                System.out.println(Instant.now() + " Waiting for client...");
                Socket clientSocket = socket.accept();
                new Connection(clientSocket).start();
            }
        }
        }

    static class Connection extends Thread {
        private final Socket clientSocket;
        private long lastSentMessageMillis;
        String nr;
        String size;
        String price;
        String din;
        String dout;
        String name;
        String rez;
        String tip;
        String stars;
        String review;
        public Connection(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run()
        {
            String nume = null;
            String pass = null;
            String actiune=null;

            try(ObjectOutputStream output = new ObjectOutputStream(clientSocket.getOutputStream());
                ObjectInputStream input = new ObjectInputStream(clientSocket.getInputStream()))
            {
                while (clientSocket.isConnected())
                {
                    if (System.currentTimeMillis() - lastSentMessageMillis > 10000)
                    {
                        System.out.println(Instant.now() + " Sending the notification to client");
                        output.writeObject("Notification");
                        lastSentMessageMillis = System.currentTimeMillis();
                    }

                    // Seems that input.available() is not reliable?
                    boolean clientHasData = clientSocket.getInputStream().available() > 0;
                    if (clientHasData) {
                        actiune = (String) input.readObject();
                        System.out.println(Instant.now() + " Got from client actiune: " + actiune);
                        if(actiune.equals("acceptReservation")){
                            nr=(String) input.readObject();
                            System.out.println(Instant.now() + " Got from client id: " + nr);
                        }
                        if(actiune.equals("loginUser")) {
                            nume = (String) input.readObject();
                            pass = (String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nume: " + nume);
                            System.out.println(Instant.now() + " Got from client parola: " + pass);
                        }
                        if(actiune.equals("CreateUser")) {
                            nume = (String) input.readObject();
                            pass = (String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nume: " + nume);
                            System.out.println(Instant.now() + " Got from client parola: " + pass);
                        }
                        if(actiune.equals("login")) {
                            nume = (String) input.readObject();
                            pass = (String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nume: " + nume);
                            System.out.println(Instant.now() + " Got from client parola: " + pass);
                        }
                        if(actiune.equals("editroom")){
                            nr = (String) input.readObject();
                            tip = (String) input.readObject();
                            price = (String) input.readObject();
                            size = (String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nr: " + nr);
                            System.out.println(Instant.now() + " Got from client tip: " + tip);
                            System.out.println(Instant.now() + " Got from client pret: " + price);
                            System.out.println(Instant.now() + " Got from client size: " + size);

                        }
                        if(actiune.equals("editbooking")){
                            nr = (String) input.readObject();
                            din = (String) input.readObject();
                            dout = (String) input.readObject();
                            name = (String) input.readObject();
                            rez = (String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nr: " + nr);
                            System.out.println(Instant.now() + " Got from client din: " + din);
                            System.out.println(Instant.now() + " Got from client dout: " + dout);
                            System.out.println(Instant.now() + " Got from client nume: " + name);
                            System.out.println(Instant.now() + " Got from client status: " + rez);

                        }
                        if(actiune.equals("listRooms")){
                            price=(String) input.readObject();
                            size=(String) input.readObject();
                            tip=(String) input.readObject();
                            System.out.println(Instant.now() + " Got from client pret: " + price);
                            System.out.println(Instant.now() + " Got from client size: " + size);
                            System.out.println(Instant.now() + " Got from client tip: " + tip);
                        }
                        if(actiune.equals("addBooking")){
                            nr=(String) input.readObject();
                            din=(String) input.readObject();
                            dout=(String) input.readObject();
                            name=(String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nr: " + nr);
                            System.out.println(Instant.now() + " Got from client din: " + din);
                            System.out.println(Instant.now() + " Got from client dout: " + dout);
                            System.out.println(Instant.now() + " Got from client nume: " + name);
                        }
                        if(actiune.equals("addRating")){
                            nr=(String) input.readObject();
                            name=(String) input.readObject();
                            stars=(String) input.readObject();
                            review=(String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nr: " + nr);
                            System.out.println(Instant.now() + " Got from client nume: " + name);
                            System.out.println(Instant.now() + " Got from client stars: " + stars);
                            System.out.println(Instant.now() + " Got from client review: " + review);
                        }
                        if(actiune.equals("addReservation")){
                            name=(String) input.readObject();
                            nr=(String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nr: " + nr);
                            System.out.println(Instant.now() + " Got from client nume: " + name);
                        }
                        if(actiune.equals("deleteReservation")){
                            //name=(String) input.readObject();
                            nr=(String) input.readObject();
                            System.out.println(Instant.now() + " Got from client nr: " + nr);
                            //System.out.println(Instant.now() + " Got from client nume: " + name);
                        }
                        // A SessionFactory is set up once for an application!
                        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                                .configure() // configures settings from hibernate.cfg.xml
                                .build();
                        try {
                            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                        }
                        catch (Exception e) {
                            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                            // so destroy it manually.
                            StandardServiceRegistryBuilder.destroy( registry );
                            System.err.println("Error in hibernate: ");
                            //e.printStackTrace();
                            return;
                        }
                        int gasit=0;
                        try(Session session = sessionFactory.openSession()) {
                            if(actiune.equals("login")) {
                                List<Administrator> admin = session.createQuery("from Administrator", Administrator.class).list();
                                for (Administrator i : admin) {
                                    //System.out.println(i+"\n");
                                    if (i.getNume().equals(nume) && i.getParola().equals(pass)) {
                                        output.writeObject("true");
                                        gasit=1;
                                        break;
                                        //CRUDController a = new CRUDController();
                                    } else {
                                        gasit=0;
                                    }
                                }
                                if(gasit==0){
                                    output.writeObject("false");
                                }
                            }
                            gasit=0;
                            if(actiune.equals("loginUser")) {
                                List<User> user = session.createQuery("from User", User.class).list();
                                for (User i : user) {
                                    //System.out.println(i+"\n");
                                    if (i.getNume().equals(nume) && i.getParola().equals(pass)) {
                                        output.writeObject("true");
                                        gasit=1;
                                        break;
                                    } else {
                                        gasit=0;
                                    }
                                }
                                if(gasit==0){
                                    output.writeObject("false");
                                }
                            }
                            if(actiune.equals("showReservation")) {
                                showRes();
                            }
                            if(actiune.equals("acceptReservation")){
                                accept(Integer.valueOf(nr));
                            }
                            if(actiune.equals("CreateUser")){
                                singup(nume,pass);
                            }
                            if(actiune.equals("listReviews")){
                                listReviews();
                            }
                            if(actiune.equals("pdf")){
                                pdfreport();
                            }
                            if(actiune.equals("txt")){
                                txtreport();
                            }
                            if(actiune.equals("editroom")){
                                editroom(Integer.valueOf(nr),Integer.valueOf(price),Integer.valueOf(size),tip);
                                output.writeObject("true");
                            }
                            if(actiune.equals("editbooking")){
                                editbooking(Integer.valueOf(nr),din,dout,name,rez);
                            }
                            if(actiune.equals("listRooms")){
                                listRoom(Integer.valueOf(price),Integer.valueOf(size),tip);
                            }
                            if(actiune.equals("addBooking")){
                                addbooking(Integer.valueOf(nr),din,dout,name);
                            }
                            if(actiune.equals("addRating")){
                                addRating(Integer.valueOf(nr),name,Integer.valueOf(stars),review);
                            }
                            if(actiune.equals("addReservation")){
                                addReservation(Integer.valueOf(nr),name);
                            }
                            if(actiune.equals("deleteReservation")){
                                deleteReservation(Integer.valueOf(nr));
                            }
                        }
                    }

                    try
                    {
                        Thread.sleep(500);
                    }
                    catch (InterruptedException e)
                    {
                        //e.printStackTrace();
                    }
                }
                System.out.println(Instant.now() + " Client disconnected?");
            }
            catch (IOException | ClassNotFoundException e)
            {
                //e.printStackTrace();
            }
            // cleanup
            try
            {
                clientSocket.close();
            }
            catch (IOException e)
            {
                //e.printStackTrace();
            }
        }

    }

    private static void singup(String nume,String pass){
        JPanel success = null;
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(new Model.User(nume,pass));
            session.getTransaction().commit();
        }
        JOptionPane.showMessageDialog(success,"Account created!");

    }

    private static void txtreport() {
        int nr;
        int size;
        int price;
        String din;
        String dout;
        String name;
        String rez;
        String tip;
        JPanel report = null;
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File("."));


        fc.setDialogTitle("Save");
        int s=fc.showSaveDialog(null);
        File f=fc.getCurrentDirectory();
        String path=f.getAbsolutePath();
        try {
            FileOutputStream file=new FileOutputStream(path+"\\Report.txt",true);
            PrintWriter pw=new PrintWriter(file);
            pw.write("Rooms table:\n");
                    //SessionFactory sessionFactory;
                    // A SessionFactory is set up once for an application!
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure() // configures settings from hibernate.cfg.xml
                    .build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            catch (Exception e) {
                        // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                        // so destroy it manually.
                        StandardServiceRegistryBuilder.destroy( registry );
                        System.err.println("Error in hibernate: ");
                        //e.printStackTrace();
                        return;
                    }
                    try(Session session = sessionFactory.openSession()) {
                        List<Room> usersHql = session.createQuery("from Room", Room.class).list();
                        for (Room i : usersHql) {
                            nr = i.getNrRoom();
                            size = i.getSize();
                            price = i.getPrice();
                            din = i.getDatain();
                            dout = i.getDataout();
                            name = i.getNume();
                            rez = i.getReserved();
                            tip = i.getType();

                                pw.write("Nr:" + nr + " Type:" + tip + " Price:" + price + " Size:" + size + " Status:" + rez + " From:" + din + " to:" + dout + " Client name:" + name + "\n");

                        }


                        pw.close();
                    }

                } catch (FileNotFoundException e) {
                    //e.printStackTrace();
                }
                JOptionPane.showMessageDialog(report,"Report created!");


    }

    private static void pdfreport() throws IOException {
        int nr;
        int size;
        int price;
        String din;
        String dout;
        String name;
        String rez;
        String tip;
        JPanel report = null;
            JFileChooser fc = new JFileChooser();
            fc.setCurrentDirectory(new File("."));
            fc.setDialogTitle("Save");
            int s = fc.showSaveDialog(null);
            File f = fc.getCurrentDirectory();
            String path = f.getAbsolutePath();
            try {
                var doc = new Document();
                PdfWriter.getInstance(doc, new FileOutputStream(path + "\\Report.pdf"));
                doc.open();
                //SessionFactory sessionFactory;
                // A SessionFactory is set up once for an application!
                final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure() // configures settings from hibernate.cfg.xml
                        .build();
                try {
                    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                } catch (Exception e) {
                    // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                    // so destroy it manually.
                    StandardServiceRegistryBuilder.destroy(registry);
                    System.err.println("Error in hibernate: ");
                    //e.printStackTrace();
                    return;
                }
                try (Session session = sessionFactory.openSession()) {
                    List<Room> usersHql = session.createQuery("from Room", Room.class).list();
                    for (Room i : usersHql) {
                        nr = i.getNrRoom();
                        size = i.getSize();
                        price = i.getPrice();
                        din = i.getDatain();
                        dout = i.getDataout();
                        name = i.getNume();
                        rez = i.getReserved();
                        tip = i.getType();
                        var p = new Paragraph("Nr:" + nr + " Type:" + tip + " Price:" + price + " Size:" + size + " Status:" + rez + " From:" + din + " to:" + dout + " Client name:" + name + "\n");
                        doc.add(p);

                    }
                    doc.close();
                }

            } catch (FileNotFoundException | DocumentException e) {
                //e.printStackTrace();
            }
        JOptionPane.showMessageDialog(report,"Report created!");
        }

    private static void editbooking(int nr,String din,String dout,String name,String cond) throws IOException {

            JPanel success = null;
            SessionFactory sessionFactory;
            // A SessionFactory is set up once for an application!
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure() // configures settings from hibernate.cfg.xml
                    .build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            } catch (Exception e) {
                // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                // so destroy it manually.
                StandardServiceRegistryBuilder.destroy(registry);
                System.err.println("Error in hibernate: ");
                //e.printStackTrace();
                return;
            }
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                List<Room> users = session.createQuery("from Room", Room.class).list();
                Room r = users.get(Integer.valueOf(nr) - 1);
                r.setNrRoom(Integer.valueOf(nr));
                r.setDatain(din);
                r.setDataout(dout);
                r.setNume(name);
                r.setReserved(cond);
                session.update(r);
                if(cond.equals("liber")){
                    List<Reservation> reservations = session.createQuery("from Reservation", Reservation.class).list();
                    for(Reservation i:reservations) {
                        if(i.getNr()==nr)
                        JOptionPane.showMessageDialog(success, i.getNume()+" room " + nr + " is free");
                        System.out.println(i.getNume()+" room " + nr + " is free");
                    }
                }
                session.getTransaction().commit();
            }


        }

    private static void editroom(int nr, int pret, int size, String tip) throws IOException {

            SessionFactory sessionFactory;
            // A SessionFactory is set up once for an application!
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure() // configures settings from hibernate.cfg.xml
                    .build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            } catch (Exception e) {
                // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                // so destroy it manually.
                StandardServiceRegistryBuilder.destroy(registry);
                System.err.println("Error in hibernate: ");
                //e.printStackTrace();
                return;
            }
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                List<Room> rooms = session.createQuery("from Room", Room.class).list();

                Room re = rooms.get(Integer.valueOf(nr) - 1);
                re.setType(tip);
                re.setPrice(Integer.valueOf(pret));
                re.setSize(Integer.valueOf(size));
                session.update(re);
                session.getTransaction().commit();
            }


        }

        private static void listRoom(int pret,int dimensiune,String type){
        JPanel success = null;
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure() // configures settings from hibernate.cfg.xml
                    .build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            catch (Exception e) {
                // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                // so destroy it manually.
                StandardServiceRegistryBuilder.destroy( registry );
                System.err.println("Error in hibernate: ");
                //e.printStackTrace();
                return;
            }
            int f=0;
            try(Session session = sessionFactory.openSession()) {
                List<Room> admin = session.createQuery("from Room", Room.class).list();
                for (Room i : admin) {
                    int nr=i.getNrRoom();
                    int price=i.getPrice();
                    String rez=i.getReserved();
                    int size=i.getSize();
                    String tip=i.getType();
                    String din=i.getDatain();
                    String dout=i.getDataout();
                    if(price<=pret && size>=dimensiune && tip.equals(type) && rez.equals("liber")) {
                        //user.write("Nr:" + nr + " Price:" + price + " Disponibilitate:" + rez + " Size:" + size + " Type:" + tip+ "\n");
                        System.out.println("Nr:" + nr + " Price:" + price + " Disponibilitate:" + rez + " Size:" + size + " Type:" + tip+ "\n");
                        f=1;
                    }
                }
            }
            if(f==0){

                JOptionPane.showMessageDialog(success,"No rooms available!");
            }

        }

        private static void addbooking(int nr,String din,String dout,String nume){
            JPanel success = null;
            final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                    .configure() // configures settings from hibernate.cfg.xml
                    .build();
            try {
                sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            }
            catch (Exception e) {
                // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                // so destroy it manually.
                StandardServiceRegistryBuilder.destroy( registry );
                System.err.println("Error in hibernate: ");
                //e.printStackTrace();
                return;
            }
            try(Session session = sessionFactory.openSession()){
                session.beginTransaction();
                List<Room> users = session.createQuery("from Room", Room.class).list();
                Room r = users.get(nr-1);
                r.setNrRoom(nr);
                r.setDatain(din);
                r.setDataout(dout);
                r.setNume(nume);
                r.setReserved("ocupat");
                session.update(r);
                session.getTransaction().commit();
            }
            JOptionPane.showMessageDialog(success,"Thank you for booking! We are waiting for you at the hotel!");

        }

        private static void addRating(int nr, String nume,int stars,String review){
            JPanel success = null;
            SessionFactory sessionFactory;
            // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }

        try(Session session = sessionFactory.openSession()) {
            //TODO: Don't forget beginTransaction/commit when doing *changes* on the data
            session.beginTransaction();
                        session.save(new Model.Rating(nume,nr,stars,review));
                        session.getTransaction().commit();

        }
            JOptionPane.showMessageDialog(success,"Rating added!");
        }

    private static void addReservation(int nr, String nume){
        JPanel success = null;
        SessionFactory sessionFactory;
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }

        try(Session session = sessionFactory.openSession()) {
            //TODO: Don't forget beginTransaction/commit when doing *changes* on the data
            int id=0;
            List<Reservation> rez = session.createQuery("from Reservation", Reservation.class).list();
            for(Reservation i:rez){
                id=i.getId();
            }
            session.beginTransaction();
            session.save(new Model.Reservation(id+1,nume,nr,"Asteptare"));
            session.getTransaction().commit();

        }
        JOptionPane.showMessageDialog(success,"Reservation added!");
    }
    private static void deleteReservation(int nr){
        JPanel success = null;
        SessionFactory sessionFactory;
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            List<Reservation> rez = session.createQuery("from Reservation", Reservation.class).list();
            Reservation re = rez.get(nr);
            re.setStatus("Reject");
            session.update(re);
            session.getTransaction().commit();

        }

        JOptionPane.showMessageDialog(success,"Reservation rejected!");
    }
    private static void listReviews(){
        JPanel success = null;
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }
        int f=0;
        try(Session session = sessionFactory.openSession()) {
            List<Rating> rating = session.createQuery("from Rating", Rating.class).list();
            for (Rating i : rating) {
                int nr=i.getNr();
                String nume=i.getNume();
                int stars=i.getStars();
                String review=i.getReview();
                    System.out.println("Nr:" + nr + " Name:" + nume + " Stars:" + stars + " Review:" + review + "\n");
                }
            }
        }
    private static void accept(int id){
        JPanel success = null;
        SessionFactory sessionFactory;
        // A SessionFactory is set up once for an application!
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            List<Reservation> rez = session.createQuery("from Reservation", Reservation.class).list();
            Reservation re = rez.get(id);
            re.setStatus("Accept");
            session.update(re);
            session.getTransaction().commit();

        }
        JOptionPane.showMessageDialog(success,"Reservation accepted!");
    }
    private static void showRes(){
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // configures settings from hibernate.cfg.xml
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
        catch (Exception e) {
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
            System.err.println("Error in hibernate: ");
            //e.printStackTrace();
            return;
        }

        try(Session session = sessionFactory.openSession()) {
            List<Reservation> res = session.createQuery("from Reservation", Reservation.class).list();
            for (Reservation i : res) {
                int id=i.getId();
                int nr=i.getNr();
                String nume=i.getNume();
                String status=i.getStatus();
                System.out.println("Id:"+id+"Nr:" + nr + " Name:" + nume + " Status:" + status + "\n");
            }
        }
    }

    }






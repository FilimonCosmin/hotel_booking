package Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="administrator")
public class Administrator {
@Id
@Column(name="name")
    private String nume;
@Column(name="pass")
    private String parola;

    public Administrator() {
        // this form used by Hibernate
    }
    public Administrator(String nume, String parola) {
        this.nume = nume;
        this.parola = parola;
    }

    public String getNume() {
        return nume;
    }
    public void setNume(String nume){
        this.nume=nume;
    }
    public String getParola() {
        return parola;
    }
    public void setParola(String parola){
        this.parola=parola;
    }

}
package Controller;

import Model.Administrator;
import Model.Room;
import View.AdminCRUD;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.xml.txw2.output.DumpSerializer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class AdminController {
    private ArrayList<Administrator> administrator;
    private ArrayList<Room> room;
    private View.Administrator v;
    private AdminCRUD adminCRUD;
    private int f=0;
    private JPanel fail;
    private JPanel success;
    private JPanel report;
    int nr;
    int size;
    int price;
    String din;
    String dout;
    String name;
    String rez;
    String tip;
    SessionFactory sessionFactory;
    public AdminController(View.Administrator v){
        this.v=v;
        addaction();
    }

    private void addaction() {
        v.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nume=v.getFormattedTextField().getText();
                String pass=v.getPasswordField().getText();
                System.out.println(nume+pass);

                // A SessionFactory is set up once for an application!
                final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure() // configures settings from hibernate.cfg.xml
                        .build();
                try {
                    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                }
                catch (Exception e) {
                    // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                    // so destroy it manually.
                    StandardServiceRegistryBuilder.destroy( registry );
                    System.err.println("Error in hibernate: ");
                    e.printStackTrace();
                    return;
                }
                try(Session session = sessionFactory.openSession()) {
                    List<Administrator> admin = session.createQuery("from Administrator", Administrator.class).list();
                    for (Administrator i : admin) {
                        //System.out.println(i+"\n");
                        if(i.getNume().equals(nume) && i.getParola().equals(pass)){
                            adminCRUD=new AdminCRUD();
                            v.close();
                            f=1;
                            editroom();
                            editbooking();
                            pdfreport();
                            txtreport();
                        }
                    }
                    if(f==0)
                    {

                        JOptionPane.showMessageDialog(fail,"Invalid Username or Password");
                    }
                    f=0;
                }

        }
        });
    }

    private void txtreport() {
        adminCRUD.getBtnNewButton_2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(new java.io.File("."));
                fc.setDialogTitle("Save");
                int s=fc.showSaveDialog(null);
                File f=fc.getCurrentDirectory();
                String path=f.getAbsolutePath();
                try {
                    FileOutputStream file=new FileOutputStream(path+"\\Report.txt",true);
                    PrintWriter pw=new PrintWriter(file);
                    pw.write("Rooms table:\n");
                    //SessionFactory sessionFactory;
                    // A SessionFactory is set up once for an application!
                    final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                            .configure() // configures settings from hibernate.cfg.xml
                            .build();
                    try {
                        sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                    }
                    catch (Exception e) {
                        // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                        // so destroy it manually.
                        StandardServiceRegistryBuilder.destroy( registry );
                        System.err.println("Error in hibernate: ");
                        e.printStackTrace();
                        return;
                    }
                    try(Session session = sessionFactory.openSession()) {
                        List<Room> usersHql = session.createQuery("from Room", Room.class).list();
                        for (Room i : usersHql) {
                            nr = i.getNrRoom();
                            size = i.getSize();
                            price = i.getPrice();
                            din = i.getDatain();
                            dout = i.getDataout();
                            name = i.getNume();
                            rez = i.getReserved();
                            tip = i.getType();

                                pw.write("Nr:" + nr + " Type:" + tip + " Price:" + price + " Size:" + size + " Status:" + rez + " From:" + din + " to:" + dout + " Client name:" + name + "\n");
                            
                        }


                        pw.close();
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                JOptionPane.showMessageDialog(report,"Report created!");
            }
        });
    }

    private void pdfreport() {
        adminCRUD.getBtnNewButton_3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(new java.io.File("."));
                fc.setDialogTitle("Save");
                int s=fc.showSaveDialog(null);
                File f=fc.getCurrentDirectory();
                String path=f.getAbsolutePath();

                try {
                    var doc=new Document();
                    PdfWriter.getInstance(doc, new FileOutputStream(path+"\\Report.pdf"));
                    doc.open();
                    //SessionFactory sessionFactory;
                    // A SessionFactory is set up once for an application!
                    final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                            .configure() // configures settings from hibernate.cfg.xml
                            .build();
                    try {
                        sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                    }
                    catch (Exception e) {
                        // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                        // so destroy it manually.
                        StandardServiceRegistryBuilder.destroy( registry );
                        System.err.println("Error in hibernate: ");
                        e.printStackTrace();
                        return;
                    }
                    try(Session session = sessionFactory.openSession()) {
                        List<Room> usersHql = session.createQuery("from Room", Room.class).list();
                        for (Room i : usersHql) {
                            nr = i.getNrRoom();
                            size = i.getSize();
                            price = i.getPrice();
                            din = i.getDatain();
                            dout = i.getDataout();
                            name = i.getNume();
                            rez = i.getReserved();
                            tip = i.getType();
                            var p=new Paragraph("Nr:"+nr+" Type:"+tip+" Price:"+price+" Size:"+size+" Status:"+rez+" From:"+din+" to:"+dout+" Client name:"+name+"\n");
                            doc.add(p);

                    }
                    doc.close();
                    }

                } catch (FileNotFoundException | DocumentException e) {
                    e.printStackTrace();
                }
                JOptionPane.showMessageDialog(report,"Report created!");
            }
        });
    }

    private void editbooking() {
        adminCRUD.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int nr=Integer.valueOf(adminCRUD.getFormattedTextField().getText());
                String tip=" ";//adminCRUD.getFormattedTextField_1().getText();
                int pret=0;//Integer.valueOf(adminCRUD.getFormattedTextField_2().getText());
                int size=0;//Integer.valueOf(adminCRUD.getFormattedTextField_3().getText());
                String din=adminCRUD.getFormattedTextField_4().getText();
                String dout=adminCRUD.getFormattedTextField_5().getText();
                String nume=adminCRUD.getFormattedTextField_6().getText();
                String cond=adminCRUD.getFormattedTextField_7().getText();
                SessionFactory sessionFactory;
                // A SessionFactory is set up once for an application!
                final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure() // configures settings from hibernate.cfg.xml
                        .build();
                try {
                    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                }
                catch (Exception e) {
                    // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                    // so destroy it manually.
                    StandardServiceRegistryBuilder.destroy( registry );
                    System.err.println("Error in hibernate: ");
                    e.printStackTrace();
                    return;
                }
                try(Session session = sessionFactory.openSession()){
                    session.beginTransaction();
                    List<Room> users = session.createQuery("from Room", Room.class).list();
                    Room r = users.get(nr-1);
                    r.setNrRoom(nr);
                    r.setDatain(din);
                    r.setDataout(dout);
                    r.setNume(nume);
                    r.setReserved(cond);
                    session.update(r);
                    session.getTransaction().commit();
                }
                JOptionPane.showMessageDialog(success,"Book edited!");
            }
        });
    }

    private void editroom() {
        adminCRUD.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int nr=Integer.valueOf(adminCRUD.getFormattedTextField().getText());
                String tip=adminCRUD.getFormattedTextField_1().getText();
                int pret=Integer.valueOf(adminCRUD.getFormattedTextField_2().getText());
                int size=Integer.valueOf(adminCRUD.getFormattedTextField_3().getText());
                String din=" ";//adminCRUD.getFormattedTextField_4().getText();
                String dout=" ";//adminCRUD.getFormattedTextField_5().getText();
                String nume=" ";//adminCRUD.getFormattedTextField_6().getText();
                String cond=" ";//adminCRUD.getFormattedTextField_7().getText();
                SessionFactory sessionFactory;
                // A SessionFactory is set up once for an application!
                final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure() // configures settings from hibernate.cfg.xml
                        .build();
                try {
                    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                }
                catch (Exception e) {
                    // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                    // so destroy it manually.
                    StandardServiceRegistryBuilder.destroy( registry );
                    System.err.println("Error in hibernate: ");
                    e.printStackTrace();
                    return;
                }
                try(Session session = sessionFactory.openSession()){
                    session.beginTransaction();
                    List<Room> rooms = session.createQuery("from Room", Room.class).list();
                    Room re = rooms.get(nr-1);
                    re.setType(tip);
                    re.setPrice(pret);
                    re.setSize(size);
                    session.update(re);
                    session.getTransaction().commit();
                }
                JOptionPane.showMessageDialog(success,"Room edited!");
            }
        });
    }
}

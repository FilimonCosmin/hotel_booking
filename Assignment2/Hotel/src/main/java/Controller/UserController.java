package Controller;

import Model.Administrator;
import Model.Room;
import View.AdminCRUD;
import View.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class UserController {
    private User user;
    private ArrayList<Room> nr;
    private JPanel success;
    SessionFactory sessionFactory;
    private int f=0;
    public UserController(User user){
        this.user=user;
        addaction();
    }
    private void addaction() {
        user.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int p=Integer.valueOf(user.getFormattedTextField_2().getText());
                int s=Integer.valueOf(user.getFormattedTextField_3().getText());
                String t=user.getFormattedTextField_1().getText();
                final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure() // configures settings from hibernate.cfg.xml
                        .build();
                try {
                    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                }
                catch (Exception e) {
                    // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                    // so destroy it manually.
                    StandardServiceRegistryBuilder.destroy( registry );
                    System.err.println("Error in hibernate: ");
                    e.printStackTrace();
                    return;
                }
                try(Session session = sessionFactory.openSession()) {
                    List<Room> admin = session.createQuery("from Room", Room.class).list();
                    for (Room i : admin) {
                        int nr=i.getNrRoom();
                        int price=i.getPrice();
                        String rez=i.getReserved();
                        int size=i.getSize();
                        String tip=i.getType();
                        String din=i.getDatain();
                        String dout=i.getDataout();
                        if(price<=p && size>=s && tip.equals(t) && rez.equals("liber")) {
                            user.write("Nr:" + nr + " Price:" + price + " Disponibilitate:" + rez + " Size:" + size + " Type:" + tip+ "\n");
                            System.out.println("Nr:" + nr + " Price:" + price + " Disponibilitate:" + rez + " Size:" + size + " Type:" + tip+ "\n");
                            f=1;
                        }
                        }
                    }
                if(f==0){
                    JOptionPane.showMessageDialog(success,"No rooms available!");
                }
            }
        });
        user.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int nr=Integer.valueOf(user.getFormattedTextField().getText());
                String tip=user.getFormattedTextField_1().getText();
                int pret=Integer.valueOf(user.getFormattedTextField_2().getText());
                int size=Integer.valueOf(user.getFormattedTextField_3().getText());
                String din=user.getFormattedTextField_4().getText();
                String dout=user.getFormattedTextField_5().getText();
                String nume=user.getFormattedTextField_6().getText();
                final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                        .configure() // configures settings from hibernate.cfg.xml
                        .build();
                try {
                    sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
                }
                catch (Exception e) {
                    // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
                    // so destroy it manually.
                    StandardServiceRegistryBuilder.destroy( registry );
                    System.err.println("Error in hibernate: ");
                    e.printStackTrace();
                    return;
                }
                try(Session session = sessionFactory.openSession()){
                    session.beginTransaction();
                    List<Room> users = session.createQuery("from Room", Room.class).list();
                    Room r = users.get(nr-1);
                    r.setNrRoom(nr);
                    r.setDatain(din);
                    r.setDataout(dout);
                    r.setNume(nume);
                    session.update(r);
                    session.getTransaction().commit();
                }
                JOptionPane.showMessageDialog(success,"Thank you for booking! We are waiting for you at the hotel!");
            }
        });

    }
}

package View;
import java.awt.EventQueue;

import javax.swing.*;
import java.awt.Font;
import java.awt.SystemColor;

public class User {

    private JFrame frame;
    private JTextField txtBooking;
    private JTextField txtRoomNumber;
    private JTextField txtType;
    private JTextField txtMaxPrice;
    private JTextField txtMinSize;
    private JTextField txtData;
    private JTextField txtCheckoutDate;
    private JTextField txtName;
    private JButton btnNewButton;
    private JButton btnNewButton_1;
    private JFormattedTextField formattedTextField;
    private JFormattedTextField formattedTextField_1;
    private JFormattedTextField formattedTextField_2;
    private JFormattedTextField formattedTextField_3;
    private JFormattedTextField formattedTextField_4;
    private JFormattedTextField formattedTextField_5;
    private JFormattedTextField formattedTextField_6;
    private JTextPane textPane;
    public User() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(UIManager.getColor("Button.darkShadow"));
        frame.setBackground(UIManager.getColor("Button.focus"));
        frame.setBounds(100, 100, 950, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        txtBooking = new JTextField();
        txtBooking.setBackground(UIManager.getColor("Button.darkShadow"));
        txtBooking.setFont(new Font("Tahoma", Font.PLAIN, 22));
        txtBooking.setText("Booking");
        txtBooking.setBounds(12, 13, 94, 33);
        frame.getContentPane().add(txtBooking);
        txtBooking.setColumns(10);

        txtRoomNumber = new JTextField();
        txtRoomNumber.setBackground(UIManager.getColor("Button.darkShadow"));
        txtRoomNumber.setText("Room number:");
        txtRoomNumber.setBounds(12, 59, 116, 22);
        frame.getContentPane().add(txtRoomNumber);
        txtRoomNumber.setColumns(10);

        txtType = new JTextField();
        txtType.setBackground(UIManager.getColor("Button.darkShadow"));
        txtType.setText("Type:");
        txtType.setBounds(12, 94, 116, 22);
        frame.getContentPane().add(txtType);
        txtType.setColumns(10);

        txtMaxPrice = new JTextField();
        txtMaxPrice.setBackground(UIManager.getColor("Button.darkShadow"));
        txtMaxPrice.setText("Max price:");
        txtMaxPrice.setBounds(12, 129, 116, 22);
        frame.getContentPane().add(txtMaxPrice);
        txtMaxPrice.setColumns(10);

        txtMinSize = new JTextField();
        txtMinSize.setBackground(UIManager.getColor("Button.darkShadow"));
        txtMinSize.setText("Min size:");
        txtMinSize.setBounds(12, 164, 116, 22);
        frame.getContentPane().add(txtMinSize);
        txtMinSize.setColumns(10);

        txtData = new JTextField();
        txtData.setBackground(UIManager.getColor("Button.darkShadow"));
        txtData.setText("Check-in date:");
        txtData.setBounds(12, 199, 116, 22);
        frame.getContentPane().add(txtData);
        txtData.setColumns(10);

        txtCheckoutDate = new JTextField();
        txtCheckoutDate.setBackground(UIManager.getColor("Button.darkShadow"));
        txtCheckoutDate.setText("Check-out date:");
        txtCheckoutDate.setBounds(250, 199, 116, 22);
        frame.getContentPane().add(txtCheckoutDate);
        txtCheckoutDate.setColumns(10);

        formattedTextField = new JFormattedTextField();
        formattedTextField.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField.setBounds(140, 59, 330, 22);
        frame.getContentPane().add(formattedTextField);

        formattedTextField_1 = new JFormattedTextField();
        formattedTextField_1.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_1.setBounds(140, 94, 330, 22);
        frame.getContentPane().add(formattedTextField_1);

        formattedTextField_2 = new JFormattedTextField();
        formattedTextField_2.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_2.setBounds(140, 129, 330, 22);
        frame.getContentPane().add(formattedTextField_2);

        formattedTextField_3 = new JFormattedTextField();
        formattedTextField_3.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_3.setBounds(140, 164, 330, 22);
        frame.getContentPane().add(formattedTextField_3);

        formattedTextField_4 = new JFormattedTextField();
        formattedTextField_4.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_4.setBounds(140, 199, 98, 22);
        frame.getContentPane().add(formattedTextField_4);

        formattedTextField_5 = new JFormattedTextField();
        formattedTextField_5.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_5.setBounds(378, 199, 92, 22);
        frame.getContentPane().add(formattedTextField_5);

        btnNewButton = new JButton("Search");
        btnNewButton.setBackground(SystemColor.controlHighlight);
        btnNewButton.setBounds(12, 265, 116, 25);
        frame.getContentPane().add(btnNewButton);

        txtName = new JTextField();
        txtName.setBackground(UIManager.getColor("Button.darkShadow"));
        txtName.setText("Name:");
        txtName.setBounds(12, 234, 116, 22);
        frame.getContentPane().add(txtName);
        txtName.setColumns(10);

        formattedTextField_6 = new JFormattedTextField();
        formattedTextField_6.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_6.setBounds(140, 234, 330, 22);
        frame.getContentPane().add(formattedTextField_6);

        btnNewButton_1 = new JButton("Book room");
        btnNewButton_1.setBackground(SystemColor.controlHighlight);
        btnNewButton_1.setBounds(141, 265, 97, 25);
        frame.getContentPane().add(btnNewButton_1);

        textPane = new JTextPane();
        textPane.setBackground(SystemColor.activeCaptionBorder);
        textPane.setBounds(482, 59, 419, 197);
        frame.getContentPane().add(textPane);

        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public JFormattedTextField getFormattedTextField() {
        return formattedTextField;
    }

    public void setFormattedTextField(JFormattedTextField formattedTextField) {
        this.formattedTextField = formattedTextField;
    }

    public JFormattedTextField getFormattedTextField_1() {
        return formattedTextField_1;
    }

    public void setFormattedTextField_1(JFormattedTextField formattedTextField_1) {
        this.formattedTextField_1 = formattedTextField_1;
    }

    public JFormattedTextField getFormattedTextField_2() {
        return formattedTextField_2;
    }

    public void setFormattedTextField_2(JFormattedTextField formattedTextField_2) {
        this.formattedTextField_2 = formattedTextField_2;
    }

    public JFormattedTextField getFormattedTextField_3() {
        return formattedTextField_3;
    }

    public void setFormattedTextField_3(JFormattedTextField formattedTextField_3) {
        this.formattedTextField_3 = formattedTextField_3;
    }

    public JFormattedTextField getFormattedTextField_4() {
        return formattedTextField_4;
    }

    public void setFormattedTextField_4(JFormattedTextField formattedTextField_4) {
        this.formattedTextField_4 = formattedTextField_4;
    }

    public JFormattedTextField getFormattedTextField_5() {
        return formattedTextField_5;
    }

    public void setFormattedTextField_5(JFormattedTextField formattedTextField_5) {
        this.formattedTextField_5 = formattedTextField_5;
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    public JFormattedTextField getFormattedTextField_6() {
        return formattedTextField_6;
    }

    public void setFormattedTextField_6(JFormattedTextField formattedTextField_6) {
        this.formattedTextField_6 = formattedTextField_6;
    }

    public void write(String s){
        textPane.setText(s);
    }
}

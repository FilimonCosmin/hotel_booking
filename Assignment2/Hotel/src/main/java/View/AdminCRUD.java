package View;
import java.awt.EventQueue;

import javax.swing.*;
import java.awt.Font;
import java.awt.SystemColor;

public class AdminCRUD {

    private JFrame frame;
    private JTextField txtAdministrator;
    private JTextField txtRoomNumber;
    private JTextField txtType;
    private JTextField txtSize;
    private JTextField txtPrice;
    private JTextField txtCheckinDate;
    private JTextField txtCheckoutDate;
    private JTextField txtClientName;
    private JTextField txtCondition;
    private JFormattedTextField formattedTextField;
    private JFormattedTextField formattedTextField_1;
    private JFormattedTextField formattedTextField_2;
    private JFormattedTextField formattedTextField_3;
    private JFormattedTextField formattedTextField_4;
    private JFormattedTextField formattedTextField_5;
    private JFormattedTextField formattedTextField_6;
    private JFormattedTextField formattedTextField_7;
    private JButton btnNewButton;
    private JButton btnNewButton_1;
    private JButton btnNewButton_2;
    private JButton btnNewButton_3;
    public AdminCRUD() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(UIManager.getColor("Button.darkShadow"));
        frame.setBackground(SystemColor.desktop);
        frame.setBounds(100, 100, 500, 370);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        txtAdministrator = new JTextField();
        txtAdministrator.setBackground(UIManager.getColor("Button.darkShadow"));
        txtAdministrator.setFont(new Font("Tahoma", Font.PLAIN, 22));
        txtAdministrator.setText("Administrator");
        txtAdministrator.setBounds(12, 13, 155, 46);
        frame.getContentPane().add(txtAdministrator);
        txtAdministrator.setColumns(10);

        txtRoomNumber = new JTextField();
        txtRoomNumber.setBackground(UIManager.getColor("Button.darkShadow"));
        txtRoomNumber.setText("Room number:");
        txtRoomNumber.setBounds(12, 72, 116, 22);
        frame.getContentPane().add(txtRoomNumber);
        txtRoomNumber.setColumns(10);

        txtType = new JTextField();
        txtType.setBackground(UIManager.getColor("Button.darkShadow"));
        txtType.setText("Type:");
        txtType.setBounds(12, 107, 116, 22);
        frame.getContentPane().add(txtType);
        txtType.setColumns(10);

        txtSize = new JTextField();
        txtSize.setBackground(UIManager.getColor("Button.darkShadow"));
        txtSize.setText("Size:");
        txtSize.setBounds(12, 142, 116, 22);
        frame.getContentPane().add(txtSize);
        txtSize.setColumns(10);

        txtPrice = new JTextField();
        txtPrice.setBackground(UIManager.getColor("Button.darkShadow"));
        txtPrice.setText("Price:");
        txtPrice.setBounds(12, 177, 116, 22);
        frame.getContentPane().add(txtPrice);
        txtPrice.setColumns(10);

        txtCheckinDate = new JTextField();
        txtCheckinDate.setBackground(UIManager.getColor("Button.darkShadow"));
        txtCheckinDate.setText("Check-in date:");
        txtCheckinDate.setBounds(12, 212, 116, 22);
        frame.getContentPane().add(txtCheckinDate);
        txtCheckinDate.setColumns(10);

        txtCheckoutDate = new JTextField();
        txtCheckoutDate.setBackground(UIManager.getColor("Button.darkShadow"));
        txtCheckoutDate.setText("Check-out date:");
        txtCheckoutDate.setBounds(250, 212, 116, 22);
        frame.getContentPane().add(txtCheckoutDate);
        txtCheckoutDate.setColumns(10);

        txtCondition = new JTextField();
        txtCondition.setBackground(UIManager.getColor("Button.darkShadow"));
        txtCondition.setText("Condition:");
        txtCondition.setBounds(250, 247, 116, 22);
        frame.getContentPane().add(txtCondition);
        txtCondition.setColumns(10);

        formattedTextField = new JFormattedTextField();
        formattedTextField.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField.setBounds(140, 72, 330, 22);
        frame.getContentPane().add(formattedTextField);

        formattedTextField_1 = new JFormattedTextField();
        formattedTextField_1.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_1.setBounds(140, 107, 330, 22);
        frame.getContentPane().add(formattedTextField_1);

        formattedTextField_2 = new JFormattedTextField();
        formattedTextField_2.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_2.setBounds(140, 142, 330, 22);
        frame.getContentPane().add(formattedTextField_2);

        formattedTextField_3 = new JFormattedTextField();
        formattedTextField_3.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_3.setBounds(140, 177, 330, 22);
        frame.getContentPane().add(formattedTextField_3);

        formattedTextField_4 = new JFormattedTextField();
        formattedTextField_4.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_4.setBounds(140, 212, 98, 22);
        frame.getContentPane().add(formattedTextField_4);

        formattedTextField_5 = new JFormattedTextField();
        formattedTextField_5.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_5.setBounds(378, 212, 92, 22);
        frame.getContentPane().add(formattedTextField_5);

        txtClientName = new JTextField();
        txtClientName.setBackground(UIManager.getColor("Button.darkShadow"));
        txtClientName.setText("Client name:");
        txtClientName.setBounds(12, 247, 116, 22);
        frame.getContentPane().add(txtClientName);
        txtClientName.setColumns(10);

        formattedTextField_6 = new JFormattedTextField();
        formattedTextField_6.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_6.setBounds(140, 247, 98, 22);
        frame.getContentPane().add(formattedTextField_6);

        formattedTextField_7 = new JFormattedTextField();
        formattedTextField_7.setBackground(SystemColor.activeCaptionBorder);
        formattedTextField_7.setBounds(378, 247, 92, 22);
        frame.getContentPane().add(formattedTextField_7);

        btnNewButton = new JButton("Edit room");
        btnNewButton.setBackground(SystemColor.controlHighlight);
        btnNewButton.setBounds(12, 282, 116, 25);
        frame.getContentPane().add(btnNewButton);

        btnNewButton_1 = new JButton("Edit booking");
        btnNewButton_1.setBackground(SystemColor.controlHighlight);
        btnNewButton_1.setBounds(141, 282, 116, 25);
        frame.getContentPane().add(btnNewButton_1);

        btnNewButton_2 = new JButton("Report txt");
        btnNewButton_2.setBackground(SystemColor.controlHighlight);
        btnNewButton_2.setBounds(269, 282, 97, 25);
        frame.getContentPane().add(btnNewButton_2);

        btnNewButton_3 = new JButton("Report pdf");
        btnNewButton_3.setBackground(SystemColor.controlHighlight);
        btnNewButton_3.setBounds(378, 282, 97, 25);
        frame.getContentPane().add(btnNewButton_3);
        frame.setVisible(true);
    }

    public JFormattedTextField getFormattedTextField() {
        return formattedTextField;
    }

    public void setFormattedTextField(JFormattedTextField formattedTextField) {
        this.formattedTextField = formattedTextField;
    }

    public JFormattedTextField getFormattedTextField_1() {
        return formattedTextField_1;
    }

    public void setFormattedTextField_1(JFormattedTextField formattedTextField_1) {
        this.formattedTextField_1 = formattedTextField_1;
    }

    public JFormattedTextField getFormattedTextField_2() {
        return formattedTextField_2;
    }

    public void setFormattedTextField_2(JFormattedTextField formattedTextField_2) {
        this.formattedTextField_2 = formattedTextField_2;
    }

    public JFormattedTextField getFormattedTextField_3() {
        return formattedTextField_3;
    }

    public void setFormattedTextField_3(JFormattedTextField formattedTextField_3) {
        this.formattedTextField_3 = formattedTextField_3;
    }

    public JFormattedTextField getFormattedTextField_4() {
        return formattedTextField_4;
    }

    public void setFormattedTextField_4(JFormattedTextField formattedTextField_4) {
        this.formattedTextField_4 = formattedTextField_4;
    }

    public JFormattedTextField getFormattedTextField_5() {
        return formattedTextField_5;
    }

    public void setFormattedTextField_5(JFormattedTextField formattedTextField_5) {
        this.formattedTextField_5 = formattedTextField_5;
    }

    public JFormattedTextField getFormattedTextField_6() {
        return formattedTextField_6;
    }

    public void setFormattedTextField_6(JFormattedTextField formattedTextField_6) {
        this.formattedTextField_6 = formattedTextField_6;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    public JButton getBtnNewButton_2() {
        return btnNewButton_2;
    }

    public void setBtnNewButton_2(JButton btnNewButton_2) {
        this.btnNewButton_2 = btnNewButton_2;
    }

    public JButton getBtnNewButton_3() {
        return btnNewButton_3;
    }

    public void setBtnNewButton_3(JButton btnNewButton_3) {
        this.btnNewButton_3 = btnNewButton_3;
    }

    public JFormattedTextField getFormattedTextField_7() {
        return formattedTextField_7;
    }

    public void setFormattedTextField_7(JFormattedTextField formattedTextField_7) {
        this.formattedTextField_7 = formattedTextField_7;
    }


}

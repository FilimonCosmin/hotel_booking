# Analysis and Design Document

# Requirement analysis

## Assignment Specification
Use JAVA/C# API to design and implement an application for the front desk employees of a bank. The application should have two types of users (a regular user represented by the front desk employee and an administrator user) which have to provide a username and a password in order to use the application.


## Function requirements
The regular user can perform the following operations:

    Add/update/view client information (name, identity card number, personal numerical code, address, etc.).
    Create/update/delete/view client account (account information: identification number, type, amount of money, date of creation).
    money between accounts.
    Process utilities bills.

The administrator user can perform the following operations:

    CRUD on employees’ information.
    Generate reports for a particular period containing the activities performed by an employee.


## Non-functional Requirements


# Use-Case Model
Create the use-case diagrams and provide one use-case description (according to the format below).

## Use case 1

    * Use case: transfer
    * Level: one of: transfer between accounts
    * Primary actor: User
    * Main success scenario: a specified amount of money is transferred from one account to another
    * Extensions: none

## Use case 2

    * Use case: view clients
    * Level: one of: lists all clients
    * Primary actor: User
    * Main success scenario: a list of customer names is displayed in the console
    * Extensions: none

## Use case 3

    * Use case: CRUD
    * Level: one of: delete update insert etc. for employees
    * Primary actor: Administrator
    * Main success scenario: perform the delete insertion update operations etc. for employees
    * Extensions: if the data entered is wrong for example The username that is unique already exists cannot be entered by a new employee

# System Architectural Design

## Architectural Pattern Description
Model–view–controller (usually known as MVC) is a software design pattern commonly used for developing user interfaces which divides the related program logic into three interconnected elements. This is done to separate internal representations of information from the ways information is presented to and accepted from the user.This kind of pattern is used for designing the layout of the page. 

## Diagrams
Create the system’s conceptual architecture; use architectural patterns and describe how they are applied. Create package, component and deployment diagrams

![diagram](DiagramBankUML.png)

# UML Sequence Diagrams
Create a sequence diagram for a relevant scenario.

# Class Design

## Design Patterns Description
MVC

## UML Class Diagram
https://upload.wikimedia.org/wikipedia/commons/a/a0/MVC-Process.svg

# Data Model
Present the data models used in the system’s implementation.

# System Testing
Present the used testing strategies (unit testing, integration testing, validation testing) and testing methods (data-flow, partitioning, boundary analysis, etc.).

# Bibliography
- [Online diagram drawing software](https://yuml.me/) ([Samples](https://yuml.me/diagram/scruffy/class/samples))
- [Yet another online diagram drawing software](https://www.draw.io)

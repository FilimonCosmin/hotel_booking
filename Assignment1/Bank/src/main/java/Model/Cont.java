package Model;

public class Cont {
    private int idCont;
    private String tip;
    private int suma;
    private String data;

    public Cont(int idCont, String tip, int suma, String data) {
        this.idCont = idCont;
        this.tip = tip;
        this.suma = suma;
        this.data=data;
    }
    public int getidCont() {

        return idCont;
    }
    public void setidCont(int idCont) {

        this.idCont = idCont;
    }

    public String getTip() {

        return tip;
    }
    public void setTip(String tip) {

        this.tip = tip;
    }
    public int getSuma() {
        return suma;
    }

    public int getIdCont() {
        return idCont;
    }

    public void setIdCont(int idCont) {
        this.idCont = idCont;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setSuma(int suma) {

        this.suma = suma;
    }

}

package Model;



public class Client {
    private int idClient;
    private String nume;
    private String prenume;
    private int cnp;
    public Client() {

    }
    public Client(int idClient, String nume, String prenume, int cnp) {
        this.idClient = idClient;
        this.nume = nume;
        this.prenume = prenume;
        this.cnp=cnp;
    }
    public int getIdClient() {
        return idClient;
    }
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
    public String getNume() {
        return nume;
    }
    public void setNume(String nume) {
        this.nume = nume;
    }
    public String getPrenume() {
        return prenume;
    }
    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }
    public int getCNP() {
        return cnp;
    }
    public void setCNP(int cnp) {
        this.cnp = cnp;
    }


}

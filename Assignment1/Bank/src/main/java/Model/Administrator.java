package Model;

public class Administrator {

    private String nume;
    private String parola;

    public Administrator(String nume, String parola) {
        this.nume = nume;
        this.parola = parola;
    }

    public String getNume() {
        return nume;
    }
    public void setNume(String nume){
        this.nume=nume;
    }
    public String getParola() {
        return parola;
    }
    public void setParola(String parola){
        this.parola=parola;
    }

}
package Controller;

import DAO.AdminDAO;
import DAO.ContDAO;
import DAO.UserDAO;
import Model.Administrator;
import Model.User;
import View.Admin;
import View.AdminCRUD;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class AdministratorController {
    private ArrayList<Administrator> x;
    private ArrayList<User> u;
    private Admin v;
    private AdminCRUD ad;
    private JPanel fail;
    private int f=0;

    public AdministratorController(Admin v){
        this.v=v;
        addaction();
    }

    private void addaction() {
       v.getBtnNewButton().addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent actionEvent) {
               x=AdminDAO.findAdmin();
               String s1=v.getPasswordField().getText();
               String s2=v.getTextField().getText();
               for(Administrator i:x){
                   if(i.getNume().equals(s2) && i.getParola().equals(s1)){

                       ad=new AdminCRUD();
                       insert();
                       delete();
                       edit();
                       report();
                       f=1;
                   }
               }
               if(f==0)
               {
                   JOptionPane.showMessageDialog(fail,"Invalid Username or Password 1");
               }
               f=0;
           }

       });

    }



    private void report() {
        ad.getBtnNewButton_2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String s1=ad.getTextField().getText();
                String s2=ad.getTextField_1().getText();
                try {
                    FileOutputStream file=new FileOutputStream("report.txt",true);
                    PrintWriter pw=new PrintWriter(file);
                    pw.write(s1+"\n");
                    pw.write("Report:");
                    pw.write(s2+"\n");
                    pw.close();
                    //System.out.println("am facut report"+s1);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void insert(){
        ad.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String s1=ad.getTextField().getText();
                String s2=ad.getPasswordField().getText();
                AdminDAO.insert(new User(s1,s2));
            }
        });
    }
    public void delete(){
        ad.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                u= UserDAO.findUser();
                String s1=ad.getTextField().getText();
                for(User i:u){
                    if(i.getNume().equals(s1)){
                        AdminDAO.delete(s1);
                    }
                }
            }
        });
    }
    public void edit(){
        ad.getBtnNewButton_3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                u= UserDAO.findUser();
                String s1=ad.getTextField().getText();
                String s2=ad.getPasswordField().getText();

                        AdminDAO.edit(new User(s1,s2));


            }
        });
    }
}

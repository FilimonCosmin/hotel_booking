package Controller;

import DAO.AdminDAO;
import DAO.ClientDAO;
import DAO.ContDAO;
import DAO.UserDAO;
import Model.Administrator;
import Model.Client;
import Model.Cont;
import Model.User;
import View.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UserController {
    private Conturi cont;
    private Clients client;
    private Transfer transfer;
    private UserGUI user;
    private ArrayList<User> u;
    private JPanel fail;
    private int f=0;
    ArrayList<Client> idClienti;
    ArrayList<Cont> idCont;
    public UserController(UserGUI user){
        this.user=user;
        action();
    }

    private void action() {
        user.getBtnLoginUser().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                u= UserDAO.findUser();
                String s1=user.getPasswordField().getText();
                String s2=user.getTextField().getText();
                for(User i:u){
                    if(i.getNume().equals(s2) && i.getParola().equals(s1)){

                        cont=new Conturi();
                        client=new Clients();
                        transfer=new Transfer();
                        insertClient();
                        deleteClient();
                        editClient();
                        viewClient();
                        insertCont();
                        deleteCont();
                        editCont();
                        viewCont();
                        transfer();
                        pay();
                        f=1;
                    }
                }
                if(f==0)
                {
                    JOptionPane.showMessageDialog(fail,"Invalid Username or Password 2");
                }
                f=0;
            }
        });
    }

    private void pay() {
        transfer.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int suma=0;
                int s1=Integer.valueOf(transfer.getTextField().getText());
                String s2="";
                int s3=Integer.valueOf(transfer.getTextField_2().getText());
                String s4="";
                idCont=ContDAO.findCont();
                for(Cont i:idCont){
                    if(i.getidCont()==s1){
                        suma=i.getSuma();
                    }

                }
                ContDAO.edit(new Cont(s1,s2,(suma-s3),s4));
            }
        });
    }


    private void transfer() {
        transfer.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int suma1=0;
                int suma2=0;
                int s=Integer.valueOf(transfer.getTextField_2().getText());
                int s1=Integer.valueOf(transfer.getTextField().getText());
                int s2=Integer.valueOf(transfer.getTextField_1().getText());
                //System.out.println("Am intrat"+s+" "+s1+" "+s2);
                String tip="";
                String data="";
                idCont=ContDAO.findCont();
                for(Cont i:idCont){
                    if(i.getidCont()==s1){
                        suma1=i.getSuma();
                        //System.out.println("Suma1"+i.getSuma());
                    }
                    if(i.getidCont()==s2){
                        suma2=i.getSuma();
                        //System.out.println("Suma2"+i.getSuma());
                    }

                }
                ContDAO.edit(new Cont(s1,tip,(suma1-s),data));
                ContDAO.edit(new Cont(s2,tip,(suma2+s),data));
            }

        });

    }

    private void viewCont() {
        cont.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                idCont=ContDAO.findCont();
                for(Cont i:idCont){
                    int s1=i.getidCont();
                    String s2=i.getTip();
                    int s3=i.getSuma();
                    String s4=i.getData();
                    System.out.println("Id:"+s1+" Tip:"+s2+" Sum:"+s3+" Data:"+s4);
                }
            }
        });
    }

    private void editCont() {
        cont.getBtnNewButton_2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int s1=Integer.valueOf(cont.getTextField().getText());
                String s2=cont.getTextField_1().getText();
                int s3=Integer.valueOf(cont.getTextField_2().getText());
                String s4=cont.getTextField_3().getText();
                ContDAO.edit(new Cont(s1,s2,s3,s4));
            }
        });
    }

    private void viewClient() {
        client.getBtnNewButton_3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                idClienti=ClientDAO.findClients();
                for(Client i:idClienti){
                    String s=i.getNume();
                    String s1=i.getPrenume();
                    System.out.println(s+" "+s1);
                }

            }
        });
    }

    private void insertCont() {
        cont.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int s1=Integer.valueOf(cont.getTextField().getText());
                String s2=cont.getTextField_1().getText();
                int s3=Integer.valueOf(cont.getTextField_2().getText());
                String s4=cont.getTextField_3().getText();
                ContDAO.insert(new Cont(s1,s2,s3,s4));
            }
        });
    }

    private void editClient() {
        client.getBtnNewButton_2().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int s1=Integer.valueOf(client.getTextField().getText());
                String s2=client.getTextField_1().getText();
                String s3=client.getTextField_2().getText();
                int s4=Integer.valueOf(client.getTextField_3().getText());
                ClientDAO.edit(new Client(s1,s2,s3,s4));
            }
        });

    }

    private void insertClient() {
        client.getBtnNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int s1=Integer.valueOf(client.getTextField().getText());
                String s2=client.getTextField_1().getText();
                String s3=client.getTextField_2().getText();
                int s4=Integer.valueOf(client.getTextField_3().getText());
                ClientDAO.insert(new Client(s1,s2,s3,s4));
            }
        });
    }
    private void deleteClient() {
        client.getBtnNewButton_1().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int s1=Integer.valueOf(client.getTextField().getText());
                        ClientDAO.delete(s1);
            }
        });
    }
    private void deleteCont() {
        cont.getBtnNewButton_3().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int s1=Integer.valueOf(cont.getTextField().getText());
                        ContDAO.delete(s1);
            }
        });
    }

}

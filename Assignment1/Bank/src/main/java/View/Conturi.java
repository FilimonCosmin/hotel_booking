package View;

import DAO.ContDAO;
import Model.Cont;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Conturi {

    private JFrame frmUserPanel;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    JButton btnNewButton;
    JButton btnNewButton_1;
    JButton btnNewButton_2;
    JButton btnNewButton_3;

    public Conturi() {
        initialize();
    }

    public JFrame getFrmUserPanel() {
        return frmUserPanel;
    }

    public void setFrmUserPanel(JFrame frmUserPanel) {
        this.frmUserPanel = frmUserPanel;
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JTextField getTextField_1() {
        return textField_1;
    }

    public void setTextField_1(JTextField textField_1) {
        this.textField_1 = textField_1;
    }

    public JTextField getTextField_2() {
        return textField_2;
    }

    public void setTextField_2(JTextField textField_2) {
        this.textField_2 = textField_2;
    }

    public JTextField getTextField_3() {
        return textField_3;
    }

    public void setTextField_3(JTextField textField_3) {
        this.textField_3 = textField_3;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    public JButton getBtnNewButton_2() {
        return btnNewButton_2;
    }

    public void setBtnNewButton_2(JButton btnNewButton_2) {
        this.btnNewButton_2 = btnNewButton_2;
    }

    public JButton getBtnNewButton_3() {
        return btnNewButton_3;
    }

    public void setBtnNewButton_3(JButton btnNewButton_3) {
        this.btnNewButton_3 = btnNewButton_3;
    }

    private void initialize() {
        frmUserPanel = new JFrame();
        frmUserPanel.setTitle("User Panel");
        frmUserPanel.setBounds(100, 100, 450, 300);
        frmUserPanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmUserPanel.getContentPane().setLayout(springLayout);

        JTextPane txtpnId = new JTextPane();
        txtpnId.setText("ID");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnId, 63, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnId, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnId, 85, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnId, 83, SpringLayout.WEST, frmUserPanel.getContentPane());
        frmUserPanel.getContentPane().add(txtpnId);

        JTextPane txtpnType = new JTextPane();
        txtpnType.setText("Type");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnType, 14, SpringLayout.SOUTH, txtpnId);
        springLayout.putConstraint(SpringLayout.WEST, txtpnType, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnType, 36, SpringLayout.SOUTH, txtpnId);
        springLayout.putConstraint(SpringLayout.EAST, txtpnType, 83, SpringLayout.WEST, frmUserPanel.getContentPane());
        frmUserPanel.getContentPane().add(txtpnType);

        JTextPane txtpnSum = new JTextPane();
        txtpnSum.setText("Sum");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnSum, 19, SpringLayout.SOUTH, txtpnType);
        springLayout.putConstraint(SpringLayout.WEST, txtpnSum, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnSum, 41, SpringLayout.SOUTH, txtpnType);
        springLayout.putConstraint(SpringLayout.EAST, txtpnSum, 83, SpringLayout.WEST, frmUserPanel.getContentPane());
        frmUserPanel.getContentPane().add(txtpnSum);

        JTextPane txtpnDate = new JTextPane();
        txtpnDate.setText("Date");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnDate, 18, SpringLayout.SOUTH, txtpnSum);
        springLayout.putConstraint(SpringLayout.WEST, txtpnDate, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnDate, 40, SpringLayout.SOUTH, txtpnSum);
        springLayout.putConstraint(SpringLayout.EAST, txtpnDate, 0, SpringLayout.EAST, txtpnId);
        frmUserPanel.getContentPane().add(txtpnDate);

        textField = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textField, 6, SpringLayout.EAST, txtpnId);
        springLayout.putConstraint(SpringLayout.SOUTH, textField, -168, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textField, 309, SpringLayout.EAST, txtpnId);
        frmUserPanel.getContentPane().add(textField);
        textField.setColumns(10);

        textField_1 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_1, 14, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.WEST, textField_1, 6, SpringLayout.EAST, txtpnType);
        springLayout.putConstraint(SpringLayout.EAST, textField_1, 0, SpringLayout.EAST, textField);
        frmUserPanel.getContentPane().add(textField_1);
        textField_1.setColumns(10);

        textField_2 = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textField_2, -343, SpringLayout.EAST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, textField_2, -91, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField);
        frmUserPanel.getContentPane().add(textField_2);
        textField_2.setColumns(10);

        textField_3 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_3, 18, SpringLayout.SOUTH, textField_2);
        springLayout.putConstraint(SpringLayout.WEST, textField_3, -343, SpringLayout.EAST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textField_3, 0, SpringLayout.EAST, textField);
        frmUserPanel.getContentPane().add(textField_3);
        textField_3.setColumns(10);

         btnNewButton = new JButton("View");

        springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -10, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        frmUserPanel.getContentPane().add(btnNewButton);

         btnNewButton_1 = new JButton("Insert");
        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 6, SpringLayout.EAST, btnNewButton);
        frmUserPanel.getContentPane().add(btnNewButton_1);

         btnNewButton_2 = new JButton("Edit");

        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_2, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_2, 11, SpringLayout.EAST, btnNewButton_1);
        frmUserPanel.getContentPane().add(btnNewButton_2);

         btnNewButton_3 = new JButton("Delete");

        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_3, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_3, 7, SpringLayout.EAST, btnNewButton_2);
        frmUserPanel.getContentPane().add(btnNewButton_3);

        JTextPane txtpnAc = new JTextPane();
        txtpnAc.setFont(new Font("Tahoma", Font.BOLD, 18));
        txtpnAc.setText("Accounts");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnAc, 10, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnAc, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnAc, 46, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnAc, 171, SpringLayout.WEST, frmUserPanel.getContentPane());
        frmUserPanel.getContentPane().add(txtpnAc);
        frmUserPanel.setVisible(true);
    }
}


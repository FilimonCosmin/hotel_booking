package View;

import DAO.AdminDAO;
import Model.User;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminCRUD {

    public JFrame frmAdministrator;
    private JTextField textField;
    private JTextField textField_1;

    public JButton getBtnNewButton_2() {
        return btnNewButton_2;
    }

    public void setBtnNewButton_2(JButton btnNewButton_2) {
        this.btnNewButton_2 = btnNewButton_2;
    }

    private JPasswordField passwordField;
    JButton btnNewButton;
    JButton btnNewButton_1;
    JButton btnNewButton_2;
    JButton btnNewButton_3;

    public AdminCRUD() {
        initialize();
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    private void initialize() {
        frmAdministrator = new JFrame();
        frmAdministrator.setTitle("Administrator");
        frmAdministrator.setBounds(100, 100, 450, 300);
        frmAdministrator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmAdministrator.getContentPane().setLayout(springLayout);

        JTextPane txtpnName = new JTextPane();
        txtpnName.setText("Name");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnName, 70, SpringLayout.NORTH, frmAdministrator.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnName, 10, SpringLayout.WEST, frmAdministrator.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnName, 92, SpringLayout.NORTH, frmAdministrator.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnName, 76, SpringLayout.WEST, frmAdministrator.getContentPane());
        frmAdministrator.getContentPane().add(txtpnName);

        btnNewButton = new JButton("Insert");
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 0, SpringLayout.WEST, txtpnName);
        springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -10, SpringLayout.SOUTH, frmAdministrator.getContentPane());
        frmAdministrator.getContentPane().add(btnNewButton);

        JTextPane txtpnPassword = new JTextPane();
        txtpnPassword.setText("Password");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnPassword, 6, SpringLayout.SOUTH, txtpnName);
        springLayout.putConstraint(SpringLayout.WEST, txtpnPassword, 0, SpringLayout.WEST, txtpnName);
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnPassword, 31, SpringLayout.SOUTH, txtpnName);
        springLayout.putConstraint(SpringLayout.EAST, txtpnPassword, 0, SpringLayout.EAST, txtpnName);
        frmAdministrator.getContentPane().add(txtpnPassword);

        btnNewButton_1 = new JButton("Delete");
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 6, SpringLayout.EAST, btnNewButton);
        springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, 0, SpringLayout.SOUTH, btnNewButton);
        frmAdministrator.getContentPane().add(btnNewButton_1);

        btnNewButton_2 = new JButton("Report");
        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_2, 0, SpringLayout.NORTH, btnNewButton);
        frmAdministrator.getContentPane().add(btnNewButton_2);

        textField = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textField, 17, SpringLayout.EAST, txtpnName);
        springLayout.putConstraint(SpringLayout.SOUTH, textField, -161, SpringLayout.SOUTH, frmAdministrator.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textField, 318, SpringLayout.EAST, txtpnName);
        frmAdministrator.getContentPane().add(textField);
        textField.setColumns(10);


        passwordField = new JPasswordField();
        springLayout.putConstraint(SpringLayout.NORTH, passwordField, 9, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.WEST, passwordField, 17, SpringLayout.EAST, txtpnPassword);
        springLayout.putConstraint(SpringLayout.EAST, passwordField, 0, SpringLayout.EAST, textField);
        frmAdministrator.getContentPane().add(passwordField);

        btnNewButton_3 = new JButton("Edit");
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_2, 6, SpringLayout.EAST, btnNewButton_3);
        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_3, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_3, 6, SpringLayout.EAST, btnNewButton_1);
        frmAdministrator.getContentPane().add(btnNewButton_3);
        frmAdministrator.setVisible(true);

        textField_1 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_1, 6, SpringLayout.SOUTH, passwordField);
        springLayout.putConstraint(SpringLayout.WEST, textField_1, 0, SpringLayout.WEST, textField);
        springLayout.putConstraint(SpringLayout.EAST, textField_1, 0, SpringLayout.EAST, textField);
        textField_1.setColumns(10);
        frmAdministrator.getContentPane().add(textField_1);

        JTextPane txtpnReport = new JTextPane();
        springLayout.putConstraint(SpringLayout.NORTH, txtpnReport, 6, SpringLayout.SOUTH, txtpnPassword);
        springLayout.putConstraint(SpringLayout.WEST, txtpnReport, 0, SpringLayout.WEST, txtpnName);
        springLayout.putConstraint(SpringLayout.EAST, txtpnReport, 0, SpringLayout.EAST, txtpnName);
        txtpnReport.setText("Report");
        frmAdministrator.getContentPane().add(txtpnReport);
    }

    public JButton getBtnNewButton_3() {
        return btnNewButton_3;
    }

    public JTextField getTextField_1() {
        return textField_1;
    }

    public void setTextField_1(JTextField textField_1) {
        this.textField_1 = textField_1;
    }

    public void setBtnNewButton_3(JButton btnNewButton_3) {
        this.btnNewButton_3 = btnNewButton_3;
    }

    public JFrame getFrmAdministrator() {
        return frmAdministrator;
    }

    public void setFrmAdministrator(JFrame frmAdministrator) {
        this.frmAdministrator = frmAdministrator;
    }

    public JTextField getTextField() {
        return textField;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(JPasswordField passwordField) {
        this.passwordField = passwordField;
    }
}

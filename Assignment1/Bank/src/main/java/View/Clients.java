package View;

import DAO.ClientDAO;
import DAO.ContDAO;
import Model.Client;
import Model.Cont;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Clients {

    private JFrame frmUserPanel;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    private JTextField textField_3;
    JButton btnNewButton;
    JButton btnNewButton_1;
    JButton btnNewButton_2;
    JButton btnNewButton_3;




    public Clients() {
        initialize();
    }

    public JFrame getFrmUserPanel() {
        return frmUserPanel;
    }

    public void setFrmUserPanel(JFrame frmUserPanel) {
        this.frmUserPanel = frmUserPanel;
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JTextField getTextField_1() {
        return textField_1;
    }

    public void setTextField_1(JTextField textField_1) {
        this.textField_1 = textField_1;
    }

    public JTextField getTextField_2() {
        return textField_2;
    }

    public void setTextField_2(JTextField textField_2) {
        this.textField_2 = textField_2;
    }

    public JTextField getTextField_3() {
        return textField_3;
    }

    public void setTextField_3(JTextField textField_3) {
        this.textField_3 = textField_3;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    public JButton getBtnNewButton_2() {
        return btnNewButton_2;
    }

    public void setBtnNewButton_2(JButton btnNewButton_2) {
        this.btnNewButton_2 = btnNewButton_2;
    }

    public JButton getBtnNewButton_3() {
        return btnNewButton_3;
    }

    public void setBtnNewButton_3(JButton btnNewButton_3) {
        this.btnNewButton_3 = btnNewButton_3;
    }

    private void initialize() {
        frmUserPanel = new JFrame();
        frmUserPanel.setTitle("User Panel");
        frmUserPanel.setBounds(100, 100, 450, 300);
        frmUserPanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        frmUserPanel.getContentPane().setLayout(springLayout);

        JTextPane txtpnFirstName = new JTextPane();
        springLayout.putConstraint(SpringLayout.NORTH, txtpnFirstName, 88, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnFirstName, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnFirstName, -342, SpringLayout.EAST, frmUserPanel.getContentPane());
        txtpnFirstName.setText("First Name");
        frmUserPanel.getContentPane().add(txtpnFirstName);

        JTextPane txtpnLastName = new JTextPane();
        txtpnLastName.setText("Last Name");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnLastName, 124, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnLastName, -99, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnFirstName, -6, SpringLayout.NORTH, txtpnLastName);
        springLayout.putConstraint(SpringLayout.WEST, txtpnLastName, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnLastName, 0, SpringLayout.EAST, txtpnFirstName);
        frmUserPanel.getContentPane().add(txtpnLastName);

        JTextPane txtpnCnp = new JTextPane();
        txtpnCnp.setText("CNP");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnCnp, 9, SpringLayout.SOUTH, txtpnLastName);
        springLayout.putConstraint(SpringLayout.WEST, txtpnCnp, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnCnp, -60, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnCnp, 0, SpringLayout.EAST, txtpnFirstName);
        frmUserPanel.getContentPane().add(txtpnCnp);

        JTextPane txtpnId = new JTextPane();
        springLayout.putConstraint(SpringLayout.NORTH, txtpnId, 60, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnId, 10, SpringLayout.WEST, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnId, -6, SpringLayout.NORTH, txtpnFirstName);
        springLayout.putConstraint(SpringLayout.EAST, txtpnId, 0, SpringLayout.EAST, txtpnFirstName);
        txtpnId.setText("ID");
        frmUserPanel.getContentPane().add(txtpnId);

        btnNewButton = new JButton("Insert");
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 0, SpringLayout.WEST, txtpnFirstName);
        springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -10, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        frmUserPanel.getContentPane().add(btnNewButton);

        btnNewButton_1 = new JButton("Delete");
        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 22, SpringLayout.EAST, btnNewButton);
        frmUserPanel.getContentPane().add(btnNewButton_1);

        btnNewButton_2 = new JButton("Edit");

        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_2, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_2, 27, SpringLayout.EAST, btnNewButton_1);
        frmUserPanel.getContentPane().add(btnNewButton_2);

        btnNewButton_3 = new JButton("View");

        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_3, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_3, 22, SpringLayout.EAST, btnNewButton_2);
        frmUserPanel.getContentPane().add(btnNewButton_3);
        textField = new JTextField();
        springLayout.putConstraint(SpringLayout.WEST, textField, 6, SpringLayout.EAST, txtpnId);
        springLayout.putConstraint(SpringLayout.SOUTH, textField, -171, SpringLayout.SOUTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, textField, 279, SpringLayout.EAST, txtpnId);
        frmUserPanel.getContentPane().add(textField);
        textField.setColumns(10);

        textField_1 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_1, 14, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.WEST, textField_1, 6, SpringLayout.EAST, txtpnFirstName);
        springLayout.putConstraint(SpringLayout.EAST, textField_1, 0, SpringLayout.EAST, textField);
        frmUserPanel.getContentPane().add(textField_1);
        textField_1.setColumns(10);

        textField_2 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_2, 14, SpringLayout.SOUTH, textField_1);
        springLayout.putConstraint(SpringLayout.WEST, textField_2, 6, SpringLayout.EAST, txtpnLastName);
        springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField);
        frmUserPanel.getContentPane().add(textField_2);
        textField_2.setColumns(10);

        textField_3 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_3, 17, SpringLayout.SOUTH, textField_2);
        springLayout.putConstraint(SpringLayout.WEST, textField_3, 6, SpringLayout.EAST, txtpnCnp);
        springLayout.putConstraint(SpringLayout.EAST, textField_3, 0, SpringLayout.EAST, textField);
        frmUserPanel.getContentPane().add(textField_3);
        textField_3.setColumns(10);

        JTextPane txtpnClients = new JTextPane();
        txtpnClients.setFont(new Font("Tahoma", Font.BOLD, 15));
        txtpnClients.setText("Clients");
        springLayout.putConstraint(SpringLayout.NORTH, txtpnClients, 10, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnClients, 0, SpringLayout.WEST, txtpnFirstName);
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnClients, 40, SpringLayout.NORTH, frmUserPanel.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnClients, 0, SpringLayout.EAST, btnNewButton_1);
        frmUserPanel.getContentPane().add(txtpnClients);
        frmUserPanel.setVisible(true);
    }

}

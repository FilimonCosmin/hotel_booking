package View;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.Font;

public class Transfer {

    private JFrame Transfer;
    private JTextField textField;
    private JTextField textField_1;
    private JTextField textField_2;
    JButton btnNewButton_1;
    JButton btnNewButton;

    public Transfer() {
        initialize();
    }

    public JButton getBtnNewButton_1() {
        return btnNewButton_1;
    }

    public void setBtnNewButton_1(JButton btnNewButton_1) {
        this.btnNewButton_1 = btnNewButton_1;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }

    private void initialize() {
        Transfer = new JFrame();
        Transfer.setTitle("User");
        Transfer.setBounds(100, 100, 410, 300);
        Transfer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SpringLayout springLayout = new SpringLayout();
        Transfer.getContentPane().setLayout(springLayout);

        textField = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField, 70, SpringLayout.NORTH, Transfer.getContentPane());
        Transfer.getContentPane().add(textField);
        textField.setColumns(10);

        textField_1 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_1, 16, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.EAST, textField, 0, SpringLayout.EAST, textField_1);
        Transfer.getContentPane().add(textField_1);
        textField_1.setColumns(10);

        textField_2 = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField_2, 21, SpringLayout.SOUTH, textField_1);
        springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField);
        Transfer.getContentPane().add(textField_2);
        textField_2.setColumns(10);

        JTextPane txtpnFrom = new JTextPane();
        springLayout.putConstraint(SpringLayout.WEST, txtpnFrom, 91, SpringLayout.WEST, Transfer.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, txtpnFrom, -161, SpringLayout.SOUTH, Transfer.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnFrom, -15, SpringLayout.WEST, textField);
        txtpnFrom.setText("From:");
        Transfer.getContentPane().add(txtpnFrom);

        JTextPane txtpnTo_1 = new JTextPane();
        springLayout.putConstraint(SpringLayout.NORTH, txtpnTo_1, 16, SpringLayout.SOUTH, txtpnFrom);
        springLayout.putConstraint(SpringLayout.WEST, txtpnTo_1, 91, SpringLayout.WEST, Transfer.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnTo_1, 0, SpringLayout.EAST, txtpnFrom);
        txtpnTo_1.setText("To:");
        Transfer.getContentPane().add(txtpnTo_1);

        JTextPane txtpnTo = new JTextPane();
        springLayout.putConstraint(SpringLayout.NORTH, txtpnTo, 21, SpringLayout.SOUTH, txtpnTo_1);
        springLayout.putConstraint(SpringLayout.WEST, txtpnTo, 91, SpringLayout.WEST, Transfer.getContentPane());
        springLayout.putConstraint(SpringLayout.EAST, txtpnTo, 0, SpringLayout.EAST, txtpnFrom);
        txtpnTo.setText("Sum:");
        Transfer.getContentPane().add(txtpnTo);

        btnNewButton = new JButton("Transfer");
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 91, SpringLayout.WEST, Transfer.getContentPane());
        springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -21, SpringLayout.SOUTH, Transfer.getContentPane());
        Transfer.getContentPane().add(btnNewButton);

        btnNewButton_1 = new JButton("Pay bill");
        springLayout.putConstraint(SpringLayout.EAST, textField_1, 0, SpringLayout.EAST, btnNewButton_1);
        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 0, SpringLayout.NORTH, btnNewButton);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 39, SpringLayout.EAST, btnNewButton);
        Transfer.getContentPane().add(btnNewButton_1);

        JTextPane txtpnTransfer = new JTextPane();
        springLayout.putConstraint(SpringLayout.NORTH, txtpnTransfer, 10, SpringLayout.NORTH, Transfer.getContentPane());
        springLayout.putConstraint(SpringLayout.WEST, txtpnTransfer, 4, SpringLayout.WEST, Transfer.getContentPane());
        txtpnTransfer.setText("Transfer");
        txtpnTransfer.setFont(new Font("Tahoma", Font.BOLD, 18));
        Transfer.getContentPane().add(txtpnTransfer);
        Transfer.setVisible(true);
    }

    public JFrame getTransfer() {
        return Transfer;
    }

    public void setTransfer(JFrame transfer) {
        Transfer = transfer;
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JTextField getTextField_1() {
        return textField_1;
    }

    public void setTextField_1(JTextField textField_1) {
        this.textField_1 = textField_1;
    }

    public JTextField getTextField_2() {
        return textField_2;
    }

    public void setTextField_2(JTextField textField_2) {
        this.textField_2 = textField_2;
    }
}

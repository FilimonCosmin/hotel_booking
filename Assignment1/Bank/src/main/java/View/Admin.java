package View;

        import javax.swing.*;
        import javax.swing.GroupLayout.Alignment;
        import javax.swing.LayoutStyle.ComponentPlacement;
        import java.awt.Font;
        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;

public class Admin {

    public JFrame frmAdmin;
    private JTextField textField;
    private JPasswordField passwordField;
    private JTextPane txtpnName;
    private JTextPane txtpnPassword;
    private JPanel fail;
    JButton btnNewButton;
    public Admin() {
        initialize();
    }

    private void initialize() {
        frmAdmin = new JFrame();
        frmAdmin.setTitle("Administrator ");
        frmAdmin.setBounds(100, 100, 450, 300);
        frmAdmin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        btnNewButton = new JButton("Login");


        textField = new JTextField();
        textField.setHorizontalAlignment(SwingConstants.CENTER);
        textField.setColumns(10);

        JTextPane txtpnUserLogin = new JTextPane();
        txtpnUserLogin.setFont(new Font("Tahoma", Font.BOLD, 18));
        txtpnUserLogin.setText("Administrator Login");

        passwordField = new JPasswordField();

        txtpnName = new JTextPane();
        txtpnName.setText("Name:");

        txtpnPassword = new JTextPane();
        txtpnPassword.setText("Password:");
        GroupLayout groupLayout = new GroupLayout(frmAdmin.getContentPane());
        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap(130, Short.MAX_VALUE)
                                .addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                                .addGap(127))
                        .addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
                                .addGap(49)
                                .addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
                                        .addComponent(txtpnPassword, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtpnName, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
                                        .addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
                                                .addComponent(txtpnUserLogin, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(textField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
                                                .addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 331, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap(52, Short.MAX_VALUE))
        );
        groupLayout.setVerticalGroup(
                groupLayout.createParallelGroup(Alignment.TRAILING)
                        .addGroup(groupLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtpnUserLogin, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(ComponentPlacement.UNRELATED)
                                .addComponent(txtpnName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(ComponentPlacement.RELATED)
                                .addComponent(txtpnPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(10)
                                .addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(26)
                                .addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
        );
        frmAdmin.getContentPane().setLayout(groupLayout);
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public void setPasswordField(JPasswordField passwordField) {
        this.passwordField = passwordField;
    }

    public JTextPane getTxtpnName() {
        return txtpnName;
    }

    public void setTxtpnName(JTextPane txtpnName) {
        this.txtpnName = txtpnName;
    }

    public JTextPane getTxtpnPassword() {
        return txtpnPassword;
    }

    public void setTxtpnPassword(JTextPane txtpnPassword) {
        this.txtpnPassword = txtpnPassword;
    }

    public JPanel getFail() {
        return fail;
    }

    public void setFail(JPanel fail) {
        this.fail = fail;
    }

    public JButton getBtnNewButton() {
        return btnNewButton;
    }

    public void setBtnNewButton(JButton btnNewButton) {
        this.btnNewButton = btnNewButton;
    }
}
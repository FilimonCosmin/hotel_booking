package DAO;

import Model.Administrator;
import Model.Client;
import Model.ConnectionFactory;

import Model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class AdminDAO {
    private static final String findStatementString = "SELECT * FROM contadmin";
    private static final String insertStatementString = "INSERT INTO user (nume,pass) VALUES (?,?)";
    private static final String deleteStatementString = "DELETE FROM user where nume = ?";
    private static final String updateStatementString = "UPDATE user SET pass=? WHERE nume=?";

    public static ArrayList<Administrator> findAdmin(){
        ArrayList<Administrator> admini=new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement=dbConnection.prepareStatement(findStatementString);
            rs=findStatement.executeQuery();
            while(rs.next()) {
                Administrator client=new Administrator(rs.getString("nume"),rs.getString("pass"));
                admini.add(client);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return admini;
    }
    public static void insert(User user){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setString(1, user.getNume());
            insertStatement.setString(2, user.getParola());
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public static void delete(String nume){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setString(1, nume);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ConnectionFactory.close(deleteStatement);
        ConnectionFactory.close(dbConnection);
    }
    public static void edit(User user) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement editStatement = null;
        try {
            editStatement = dbConnection.prepareStatement(updateStatementString);
            editStatement.setString(1, user.getParola());
            editStatement.setString(2, user.getNume());
            editStatement.executeUpdate();

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(editStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}

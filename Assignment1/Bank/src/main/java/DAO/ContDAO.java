package DAO;


import Model.ConnectionFactory;
import Model.Cont;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ContDAO {
    private static final String findStatementString = "SELECT * FROM cont";
    private static final String insertStatementString = "INSERT INTO cont (idCont,tip,suma,data) VALUES (?,?,?,?)";
    private static final String deleteStatementString = "DELETE FROM cont where idCont = ?";
    private static final String updateStatementString = "UPDATE cont SET suma=? WHERE idCont=?";
    private static final String transferStatementString = "SELECT suma FROM cont WHERE idCont=?";


    public static ArrayList<Cont> findCont(){
        ArrayList<Cont> conti=new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement=dbConnection.prepareStatement(findStatementString);
            rs=findStatement.executeQuery();
            while(rs.next()) {
                Cont cont=new Cont(rs.getInt("idCont"),rs.getString("tip"),rs.getInt("suma"),rs.getString("data"));
                conti.add(cont);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return conti;
    }
    public static void insert(Cont cont){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, cont.getidCont());
            insertStatement.setString(2, cont.getTip());
            insertStatement.setInt(3, cont.getSuma());
            insertStatement.setString(4, cont.getData());
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public static void delete(int id){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ConnectionFactory.close(deleteStatement);
        ConnectionFactory.close(dbConnection);
    }
        public static void edit(Cont cont) {
            Connection dbConnection = ConnectionFactory.getConnection();
            PreparedStatement editStatement = null;
            try {
                editStatement = dbConnection.prepareStatement(updateStatementString);
                editStatement.setInt(2, cont.getidCont());
               // editStatement.setString(1, cont.getTip());
                editStatement.setInt(1, cont.getSuma());
               // editStatement.setString(3, cont.getData());
                editStatement.executeUpdate();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            finally {
                ConnectionFactory.close(editStatement);
                ConnectionFactory.close(dbConnection);
            }
        }
}

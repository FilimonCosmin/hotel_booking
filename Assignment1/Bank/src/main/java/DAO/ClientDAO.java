package DAO;

import Model.Client;
import Model.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClientDAO {
    private static final String findStatementString = "SELECT * FROM client";
    private static final String insertStatementString = "INSERT INTO client (idClient,nume,prenume,cnp) VALUES (?,?,?,?)";
    private static final String deleteStatementString = "DELETE FROM client where idClient = ?";
    private static final String updateStatementString = "UPDATE client SET nume=?, prenume=?, cnp=? WHERE idClient=?";

    public static ArrayList<Client> findClients(){
        ArrayList<Client> clienti=new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement=dbConnection.prepareStatement(findStatementString);
            rs=findStatement.executeQuery();
            while(rs.next()) {
                Client client=new Client(rs.getInt("idClient"),rs.getString("nume"),rs.getString("prenume"),rs.getInt("cnp"));
                clienti.add(client);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return clienti;
    }

    public static void insert(Client client){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement insertStatement = null;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString);
            insertStatement.setInt(1, client.getIdClient());
            insertStatement.setString(2, client.getNume());
            insertStatement.setString(3, client.getPrenume());
            insertStatement.setInt(4, client.getCNP());
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static void delete(int id){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        ConnectionFactory.close(deleteStatement);
        ConnectionFactory.close(dbConnection);
    }

    public static void edit(Client client){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement editStatement = null;
        try {
            editStatement = dbConnection.prepareStatement(updateStatementString);
            editStatement.setInt(4, client.getIdClient());
            editStatement.setString(1, client.getNume());
            editStatement.setString(2, client.getPrenume());
            editStatement.setInt(3, client.getCNP());
            editStatement.executeUpdate();
            System.out.println(editStatement);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(editStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}

package DAO;

import Model.Administrator;
import Model.ConnectionFactory;
import Model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDAO {
    private static final String findStatementString = "SELECT * FROM user";
    public static ArrayList<User> findUser(){
        ArrayList<User> useri=new ArrayList<>();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement=dbConnection.prepareStatement(findStatementString);
            rs=findStatement.executeQuery();
            while(rs.next()) {
                User user=new User(rs.getString("nume"),rs.getString("pass"));
                useri.add(user);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return useri;
    }
}
